// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME BplusxAODAnalysisLibCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "BplusxAODAnalysis/BaBarAngles.h"
#include "BplusxAODAnalysis/BplusNtupleMaker.h"
#include "BplusxAODAnalysis/MuonDump.h"
#include "BplusxAODAnalysis/ParticleGun.h"
#include "BplusxAODAnalysis/SpecificEvents.h"
#include "BplusxAODAnalysis/TriggerTest.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_ParticleGun(void *p = 0);
   static void *newArray_ParticleGun(Long_t size, void *p);
   static void delete_ParticleGun(void *p);
   static void deleteArray_ParticleGun(void *p);
   static void destruct_ParticleGun(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ParticleGun*)
   {
      ::ParticleGun *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ParticleGun >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ParticleGun", ::ParticleGun::Class_Version(), "BplusxAODAnalysis/ParticleGun.h", 10,
                  typeid(::ParticleGun), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::ParticleGun::Dictionary, isa_proxy, 4,
                  sizeof(::ParticleGun) );
      instance.SetNew(&new_ParticleGun);
      instance.SetNewArray(&newArray_ParticleGun);
      instance.SetDelete(&delete_ParticleGun);
      instance.SetDeleteArray(&deleteArray_ParticleGun);
      instance.SetDestructor(&destruct_ParticleGun);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ParticleGun*)
   {
      return GenerateInitInstanceLocal((::ParticleGun*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ParticleGun*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_SpecificEvents(void *p = 0);
   static void *newArray_SpecificEvents(Long_t size, void *p);
   static void delete_SpecificEvents(void *p);
   static void deleteArray_SpecificEvents(void *p);
   static void destruct_SpecificEvents(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SpecificEvents*)
   {
      ::SpecificEvents *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SpecificEvents >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SpecificEvents", ::SpecificEvents::Class_Version(), "BplusxAODAnalysis/SpecificEvents.h", 25,
                  typeid(::SpecificEvents), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::SpecificEvents::Dictionary, isa_proxy, 4,
                  sizeof(::SpecificEvents) );
      instance.SetNew(&new_SpecificEvents);
      instance.SetNewArray(&newArray_SpecificEvents);
      instance.SetDelete(&delete_SpecificEvents);
      instance.SetDeleteArray(&deleteArray_SpecificEvents);
      instance.SetDestructor(&destruct_SpecificEvents);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SpecificEvents*)
   {
      return GenerateInitInstanceLocal((::SpecificEvents*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::SpecificEvents*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MuonDump(void *p = 0);
   static void *newArray_MuonDump(Long_t size, void *p);
   static void delete_MuonDump(void *p);
   static void deleteArray_MuonDump(void *p);
   static void destruct_MuonDump(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonDump*)
   {
      ::MuonDump *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonDump >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonDump", ::MuonDump::Class_Version(), "BplusxAODAnalysis/MuonDump.h", 7,
                  typeid(::MuonDump), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::MuonDump::Dictionary, isa_proxy, 4,
                  sizeof(::MuonDump) );
      instance.SetNew(&new_MuonDump);
      instance.SetNewArray(&newArray_MuonDump);
      instance.SetDelete(&delete_MuonDump);
      instance.SetDeleteArray(&deleteArray_MuonDump);
      instance.SetDestructor(&destruct_MuonDump);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonDump*)
   {
      return GenerateInitInstanceLocal((::MuonDump*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::MuonDump*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TriggerTest(void *p = 0);
   static void *newArray_TriggerTest(Long_t size, void *p);
   static void delete_TriggerTest(void *p);
   static void deleteArray_TriggerTest(void *p);
   static void destruct_TriggerTest(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TriggerTest*)
   {
      ::TriggerTest *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TriggerTest >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TriggerTest", ::TriggerTest::Class_Version(), "BplusxAODAnalysis/TriggerTest.h", 21,
                  typeid(::TriggerTest), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TriggerTest::Dictionary, isa_proxy, 4,
                  sizeof(::TriggerTest) );
      instance.SetNew(&new_TriggerTest);
      instance.SetNewArray(&newArray_TriggerTest);
      instance.SetDelete(&delete_TriggerTest);
      instance.SetDeleteArray(&deleteArray_TriggerTest);
      instance.SetDestructor(&destruct_TriggerTest);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TriggerTest*)
   {
      return GenerateInitInstanceLocal((::TriggerTest*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TriggerTest*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_BplusNtupleMaker(void *p = 0);
   static void *newArray_BplusNtupleMaker(Long_t size, void *p);
   static void delete_BplusNtupleMaker(void *p);
   static void deleteArray_BplusNtupleMaker(void *p);
   static void destruct_BplusNtupleMaker(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BplusNtupleMaker*)
   {
      ::BplusNtupleMaker *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::BplusNtupleMaker >(0);
      static ::ROOT::TGenericClassInfo 
         instance("BplusNtupleMaker", ::BplusNtupleMaker::Class_Version(), "BplusxAODAnalysis/BplusNtupleMaker.h", 44,
                  typeid(::BplusNtupleMaker), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::BplusNtupleMaker::Dictionary, isa_proxy, 4,
                  sizeof(::BplusNtupleMaker) );
      instance.SetNew(&new_BplusNtupleMaker);
      instance.SetNewArray(&newArray_BplusNtupleMaker);
      instance.SetDelete(&delete_BplusNtupleMaker);
      instance.SetDeleteArray(&deleteArray_BplusNtupleMaker);
      instance.SetDestructor(&destruct_BplusNtupleMaker);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BplusNtupleMaker*)
   {
      return GenerateInitInstanceLocal((::BplusNtupleMaker*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::BplusNtupleMaker*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr ParticleGun::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ParticleGun::Class_Name()
{
   return "ParticleGun";
}

//______________________________________________________________________________
const char *ParticleGun::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ParticleGun*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ParticleGun::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ParticleGun*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ParticleGun::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ParticleGun*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ParticleGun::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ParticleGun*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr SpecificEvents::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *SpecificEvents::Class_Name()
{
   return "SpecificEvents";
}

//______________________________________________________________________________
const char *SpecificEvents::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SpecificEvents*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SpecificEvents::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SpecificEvents*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *SpecificEvents::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SpecificEvents*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *SpecificEvents::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SpecificEvents*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MuonDump::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MuonDump::Class_Name()
{
   return "MuonDump";
}

//______________________________________________________________________________
const char *MuonDump::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonDump*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MuonDump::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonDump*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MuonDump::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonDump*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MuonDump::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonDump*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TriggerTest::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TriggerTest::Class_Name()
{
   return "TriggerTest";
}

//______________________________________________________________________________
const char *TriggerTest::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TriggerTest*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TriggerTest::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TriggerTest*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TriggerTest::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TriggerTest*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TriggerTest::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TriggerTest*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr BplusNtupleMaker::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *BplusNtupleMaker::Class_Name()
{
   return "BplusNtupleMaker";
}

//______________________________________________________________________________
const char *BplusNtupleMaker::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BplusNtupleMaker*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int BplusNtupleMaker::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BplusNtupleMaker*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BplusNtupleMaker::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BplusNtupleMaker*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BplusNtupleMaker::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BplusNtupleMaker*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void ParticleGun::Streamer(TBuffer &R__b)
{
   // Stream an object of class ParticleGun.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ParticleGun::Class(),this);
   } else {
      R__b.WriteClassBuffer(ParticleGun::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ParticleGun(void *p) {
      return  p ? new(p) ::ParticleGun : new ::ParticleGun;
   }
   static void *newArray_ParticleGun(Long_t nElements, void *p) {
      return p ? new(p) ::ParticleGun[nElements] : new ::ParticleGun[nElements];
   }
   // Wrapper around operator delete
   static void delete_ParticleGun(void *p) {
      delete ((::ParticleGun*)p);
   }
   static void deleteArray_ParticleGun(void *p) {
      delete [] ((::ParticleGun*)p);
   }
   static void destruct_ParticleGun(void *p) {
      typedef ::ParticleGun current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ParticleGun

//______________________________________________________________________________
void SpecificEvents::Streamer(TBuffer &R__b)
{
   // Stream an object of class SpecificEvents.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SpecificEvents::Class(),this);
   } else {
      R__b.WriteClassBuffer(SpecificEvents::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_SpecificEvents(void *p) {
      return  p ? new(p) ::SpecificEvents : new ::SpecificEvents;
   }
   static void *newArray_SpecificEvents(Long_t nElements, void *p) {
      return p ? new(p) ::SpecificEvents[nElements] : new ::SpecificEvents[nElements];
   }
   // Wrapper around operator delete
   static void delete_SpecificEvents(void *p) {
      delete ((::SpecificEvents*)p);
   }
   static void deleteArray_SpecificEvents(void *p) {
      delete [] ((::SpecificEvents*)p);
   }
   static void destruct_SpecificEvents(void *p) {
      typedef ::SpecificEvents current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SpecificEvents

//______________________________________________________________________________
void MuonDump::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonDump.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonDump::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonDump::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonDump(void *p) {
      return  p ? new(p) ::MuonDump : new ::MuonDump;
   }
   static void *newArray_MuonDump(Long_t nElements, void *p) {
      return p ? new(p) ::MuonDump[nElements] : new ::MuonDump[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonDump(void *p) {
      delete ((::MuonDump*)p);
   }
   static void deleteArray_MuonDump(void *p) {
      delete [] ((::MuonDump*)p);
   }
   static void destruct_MuonDump(void *p) {
      typedef ::MuonDump current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonDump

//______________________________________________________________________________
void TriggerTest::Streamer(TBuffer &R__b)
{
   // Stream an object of class TriggerTest.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TriggerTest::Class(),this);
   } else {
      R__b.WriteClassBuffer(TriggerTest::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TriggerTest(void *p) {
      return  p ? new(p) ::TriggerTest : new ::TriggerTest;
   }
   static void *newArray_TriggerTest(Long_t nElements, void *p) {
      return p ? new(p) ::TriggerTest[nElements] : new ::TriggerTest[nElements];
   }
   // Wrapper around operator delete
   static void delete_TriggerTest(void *p) {
      delete ((::TriggerTest*)p);
   }
   static void deleteArray_TriggerTest(void *p) {
      delete [] ((::TriggerTest*)p);
   }
   static void destruct_TriggerTest(void *p) {
      typedef ::TriggerTest current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TriggerTest

//______________________________________________________________________________
void BplusNtupleMaker::Streamer(TBuffer &R__b)
{
   // Stream an object of class BplusNtupleMaker.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(BplusNtupleMaker::Class(),this);
   } else {
      R__b.WriteClassBuffer(BplusNtupleMaker::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_BplusNtupleMaker(void *p) {
      return  p ? new(p) ::BplusNtupleMaker : new ::BplusNtupleMaker;
   }
   static void *newArray_BplusNtupleMaker(Long_t nElements, void *p) {
      return p ? new(p) ::BplusNtupleMaker[nElements] : new ::BplusNtupleMaker[nElements];
   }
   // Wrapper around operator delete
   static void delete_BplusNtupleMaker(void *p) {
      delete ((::BplusNtupleMaker*)p);
   }
   static void deleteArray_BplusNtupleMaker(void *p) {
      delete [] ((::BplusNtupleMaker*)p);
   }
   static void destruct_BplusNtupleMaker(void *p) {
      typedef ::BplusNtupleMaker current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BplusNtupleMaker

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 214,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEULong64_tgR_Dictionary();
   static void vectorlEULong64_tgR_TClassManip(TClass*);
   static void *new_vectorlEULong64_tgR(void *p = 0);
   static void *newArray_vectorlEULong64_tgR(Long_t size, void *p);
   static void delete_vectorlEULong64_tgR(void *p);
   static void deleteArray_vectorlEULong64_tgR(void *p);
   static void destruct_vectorlEULong64_tgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ULong64_t>*)
   {
      vector<ULong64_t> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ULong64_t>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ULong64_t>", -2, "vector", 214,
                  typeid(vector<ULong64_t>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEULong64_tgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ULong64_t>) );
      instance.SetNew(&new_vectorlEULong64_tgR);
      instance.SetNewArray(&newArray_vectorlEULong64_tgR);
      instance.SetDelete(&delete_vectorlEULong64_tgR);
      instance.SetDeleteArray(&deleteArray_vectorlEULong64_tgR);
      instance.SetDestructor(&destruct_vectorlEULong64_tgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ULong64_t> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEULong64_tgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ULong64_t>*)0x0)->GetClass();
      vectorlEULong64_tgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEULong64_tgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEULong64_tgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t> : new vector<ULong64_t>;
   }
   static void *newArray_vectorlEULong64_tgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ULong64_t>[nElements] : new vector<ULong64_t>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEULong64_tgR(void *p) {
      delete ((vector<ULong64_t>*)p);
   }
   static void deleteArray_vectorlEULong64_tgR(void *p) {
      delete [] ((vector<ULong64_t>*)p);
   }
   static void destruct_vectorlEULong64_tgR(void *p) {
      typedef vector<ULong64_t> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ULong64_t>

namespace ROOT {
   static TClass *vectorlEScalarBranchVariablelEintgRmUgR_Dictionary();
   static void vectorlEScalarBranchVariablelEintgRmUgR_TClassManip(TClass*);
   static void *new_vectorlEScalarBranchVariablelEintgRmUgR(void *p = 0);
   static void *newArray_vectorlEScalarBranchVariablelEintgRmUgR(Long_t size, void *p);
   static void delete_vectorlEScalarBranchVariablelEintgRmUgR(void *p);
   static void deleteArray_vectorlEScalarBranchVariablelEintgRmUgR(void *p);
   static void destruct_vectorlEScalarBranchVariablelEintgRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<ScalarBranchVariable<int>*>*)
   {
      vector<ScalarBranchVariable<int>*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<ScalarBranchVariable<int>*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<ScalarBranchVariable<int>*>", -2, "vector", 214,
                  typeid(vector<ScalarBranchVariable<int>*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEScalarBranchVariablelEintgRmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<ScalarBranchVariable<int>*>) );
      instance.SetNew(&new_vectorlEScalarBranchVariablelEintgRmUgR);
      instance.SetNewArray(&newArray_vectorlEScalarBranchVariablelEintgRmUgR);
      instance.SetDelete(&delete_vectorlEScalarBranchVariablelEintgRmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEScalarBranchVariablelEintgRmUgR);
      instance.SetDestructor(&destruct_vectorlEScalarBranchVariablelEintgRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<ScalarBranchVariable<int>*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<ScalarBranchVariable<int>*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEScalarBranchVariablelEintgRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<ScalarBranchVariable<int>*>*)0x0)->GetClass();
      vectorlEScalarBranchVariablelEintgRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEScalarBranchVariablelEintgRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEScalarBranchVariablelEintgRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ScalarBranchVariable<int>*> : new vector<ScalarBranchVariable<int>*>;
   }
   static void *newArray_vectorlEScalarBranchVariablelEintgRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<ScalarBranchVariable<int>*>[nElements] : new vector<ScalarBranchVariable<int>*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEScalarBranchVariablelEintgRmUgR(void *p) {
      delete ((vector<ScalarBranchVariable<int>*>*)p);
   }
   static void deleteArray_vectorlEScalarBranchVariablelEintgRmUgR(void *p) {
      delete [] ((vector<ScalarBranchVariable<int>*>*)p);
   }
   static void destruct_vectorlEScalarBranchVariablelEintgRmUgR(void *p) {
      typedef vector<ScalarBranchVariable<int>*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<ScalarBranchVariable<int>*>

namespace ROOT {
   static TClass *maplEunsignedsPintcOvectorlEULong64_tgRsPgR_Dictionary();
   static void maplEunsignedsPintcOvectorlEULong64_tgRsPgR_TClassManip(TClass*);
   static void *new_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p = 0);
   static void *newArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(Long_t size, void *p);
   static void delete_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p);
   static void deleteArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p);
   static void destruct_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<unsigned int,vector<ULong64_t> >*)
   {
      map<unsigned int,vector<ULong64_t> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<unsigned int,vector<ULong64_t> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<unsigned int,vector<ULong64_t> >", -2, "map", 96,
                  typeid(map<unsigned int,vector<ULong64_t> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEunsignedsPintcOvectorlEULong64_tgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<unsigned int,vector<ULong64_t> >) );
      instance.SetNew(&new_maplEunsignedsPintcOvectorlEULong64_tgRsPgR);
      instance.SetNewArray(&newArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR);
      instance.SetDelete(&delete_maplEunsignedsPintcOvectorlEULong64_tgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR);
      instance.SetDestructor(&destruct_maplEunsignedsPintcOvectorlEULong64_tgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<unsigned int,vector<ULong64_t> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<unsigned int,vector<ULong64_t> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEunsignedsPintcOvectorlEULong64_tgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<unsigned int,vector<ULong64_t> >*)0x0)->GetClass();
      maplEunsignedsPintcOvectorlEULong64_tgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEunsignedsPintcOvectorlEULong64_tgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned int,vector<ULong64_t> > : new map<unsigned int,vector<ULong64_t> >;
   }
   static void *newArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<unsigned int,vector<ULong64_t> >[nElements] : new map<unsigned int,vector<ULong64_t> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p) {
      delete ((map<unsigned int,vector<ULong64_t> >*)p);
   }
   static void deleteArray_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p) {
      delete [] ((map<unsigned int,vector<ULong64_t> >*)p);
   }
   static void destruct_maplEunsignedsPintcOvectorlEULong64_tgRsPgR(void *p) {
      typedef map<unsigned int,vector<ULong64_t> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<unsigned int,vector<ULong64_t> >

namespace {
  void TriggerDictionaryInitialization_libBplusxAODAnalysisLib_Impl() {
    static const char* headers[] = {
"BplusxAODAnalysis/BaBarAngles.h",
"BplusxAODAnalysis/BplusNtupleMaker.h",
"BplusxAODAnalysis/MuonDump.h",
"BplusxAODAnalysis/ParticleGun.h",
"BplusxAODAnalysis/SpecificEvents.h",
"BplusxAODAnalysis/TriggerTest.h",
0
    };
    static const char* includePaths[] = {
"/data0/novotnyr/BplusNtuples/source/BplusxAODAnalysis",
"/data0/novotnyr/BplusNtuples/source/BplusxAODAnalysis",
"/data0/novotnyr/BplusNtuples/source/BplusxAODAnalysis",
"/data0/novotnyr/BplusNtuples/source/BplusxAODAnalysis",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBPhys",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/data0/novotnyr/BplusNtuples/source/BPhysxAODTools",
"/data0/novotnyr/BplusNtuples/source/BPhysxAODTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODTrigBphys",
"/data0/novotnyr/BplusNtuples/source/BChargeFlavourTagTool",
"/data0/novotnyr/BplusNtuples/source/BChargeFlavourTagTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/../../../../AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.73/InstallArea/x86_64-slc6-gcc62-opt/include",
"/data0/novotnyr/BplusNtuples/source/BplusxAODAnalysis/CMakeFiles/makeBplusxAODAnalysisLibCintDict.4GnPW0/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libBplusxAODAnalysisLib dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$BplusxAODAnalysis/ParticleGun.h")))  ParticleGun;
class __attribute__((annotate("$clingAutoload$BplusxAODAnalysis/SpecificEvents.h")))  SpecificEvents;
class __attribute__((annotate("$clingAutoload$BplusxAODAnalysis/MuonDump.h")))  MuonDump;
class __attribute__((annotate("$clingAutoload$BplusxAODAnalysis/TriggerTest.h")))  TriggerTest;
class __attribute__((annotate("$clingAutoload$BplusxAODAnalysis/BplusNtupleMaker.h")))  BplusNtupleMaker;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libBplusxAODAnalysisLib dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "BplusxAODAnalysis-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ BplusxAODAnalysis-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "BplusxAODAnalysis/BaBarAngles.h"
#include "BplusxAODAnalysis/BplusNtupleMaker.h"
#include "BplusxAODAnalysis/MuonDump.h"
#include "BplusxAODAnalysis/ParticleGun.h"
#include "BplusxAODAnalysis/SpecificEvents.h"
#include "BplusxAODAnalysis/TriggerTest.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"BplusNtupleMaker", payloadCode, "@",
"MuonDump", payloadCode, "@",
"ParticleGun", payloadCode, "@",
"SpecificEvents", payloadCode, "@",
"TriggerTest", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libBplusxAODAnalysisLib",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libBplusxAODAnalysisLib_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libBplusxAODAnalysisLib_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libBplusxAODAnalysisLib() {
  TriggerDictionaryInitialization_libBplusxAODAnalysisLib_Impl();
}
