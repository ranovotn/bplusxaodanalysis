#ifndef BaBarAngles_H
#define BaBarAngles_H

#include "TVector3.h"
#include "TLorentzVector.h"
#include "TMath.h"
class BaBarAngles{

 
private:

    // Member data
  double _theta1;
  double _theta2;
  double _chi;
  double _thetatr;
  double _phitr;

public:
  double theta1()  const { return _theta1;  }
  double theta2()  const { return _theta2;  }
  double chi()     const { return _chi;     }
  double thetatr() const { return _thetatr; }
  double phitr()   const { return _phitr;   }
 
  double theta1fix()  const { return TMath::Cos(_theta1);  }
  double theta2fix()  const { return TMath::Cos(_theta2);  }
  double chifix()     const { return -(_chi -TMath::Pi()) ;     }
  double thetatrfix() const { return -TMath::Cos(_thetatr); }
  double phitrfix()   const { return -(_phitr - TMath::Pi());   }  

//      Input:
//            For J/Psi(l+l-) K*(K pi):
//             o cand1 = K    (K from K* decay)
//             o cand2 = l+   (positive lepton from psi decay)
//             o aBcand = B   (the B from which the decay originates)

  BaBarAngles(const TLorentzVector& Kplus, const TLorentzVector& Muplus,
                const TLorentzVector& Phi, const TLorentzVector& Jpsi
                , const TLorentzVector& Bs){
               
     _theta1  = 0.;
     _theta2  = 0.;
     _chi     = 0.;
     _thetatr = 0.;
     _phitr   = 0.;
     
     
       // 0.1 Get the Mummy1 & Mummy2 4-vectors in lab
  TLorentzVector Mum1QV ( Phi ) ; // this should be phi
  TLorentzVector Mum2QV ( Jpsi ) ; // this should be jpsi
  // 0.2 Boost the Mummy1 & Mummy2 to the B(aBcand) rest frame
  TLorentzVector BQV(Bs); // this should be Bs Meson

  TVector3 Bboost ( BQV.BoostVector() );
  Mum1QV.Boost ( -( Bboost ) );
  Mum2QV.Boost ( -( Bboost ) );
    // 1. Compute helicity angle theta1
  // 1.1 Boost cand1 to the B rest frame (grandMa)
  TLorentzVector cand1QV ( Kplus );
  cand1QV.Boost ( - (Bboost) );
  // 1.2 Boost now cand1 in his mother rest frame
  cand1QV.Boost ( - (Mum1QV.BoostVector() ) );
  // 1.3 compute the theta1 angle
  TVector3 Mum1TV = Mum1QV.BoostVector();
  TVector3 cand1TV = cand1QV.BoostVector();
  TVector3 ucand1TV = cand1TV.Unit();
  _theta1 = cand1TV.Angle(Mum1TV);
 
 
 
  // 2. Compute helicity angle theta2
  // 2.1 Boost cand2 to the B rest frame (grandMa)
  TLorentzVector cand2QV ( Muplus );
  cand2QV.Boost ( - (Bboost) );
  // 2.2 Boost now cand2 in his mother rest frame
  cand2QV.Boost ( - (Mum2QV.BoostVector() ) );
  // 2.3 compute the theta2 angle
  TVector3 Mum2TV = Mum2QV.BoostVector();
  TVector3 cand2TV = cand2QV.BoostVector();
  TVector3 ucand2TV = cand2TV.Unit();
  _theta2 = cand2TV.Angle(Mum2TV);

  // 3. Compute the chi angle
  // 3.1 Define the c and d vectors (see Physbook)
  TVector3 c;
  TVector3 d;
  TVector3 uMum1TV = Mum1TV.Unit();
  TVector3 uMum2TV = Mum2TV.Unit();

  // 3.2 Calculate these vectors
  c = (uMum1TV.Cross(ucand1TV)).Cross(uMum1TV);
  d = (uMum2TV.Cross(ucand2TV)).Cross(uMum2TV);
  double sinphi = (c.Cross(uMum1TV)).Dot(d);
  double cosphi = c.Dot(d);
  // 3.3 Now get chi = angle between the 2 decay planes
  _chi = atan2(sinphi,cosphi);
  // 3.4 Put chi in [0;2pi]
  if ( _chi < 0 )
  {
    _chi = 2.*M_PI + _chi;
  }
 
  // 4. Transversity angles
  double cosThetaTr = sin ( _theta2 ) * sin ( _chi ) ;
  double sinThetaTr = sqrt ( 1. - cosThetaTr * cosThetaTr ) ;
  double cosPhiTr = cos(_theta2) / sinThetaTr ;
  double sinPhiTr = sin(_theta2) * cos( _chi ) / sinThetaTr ;
  _phitr   = atan2 ( sinPhiTr, cosPhiTr ) ;
  if (_phitr < 0)
  {
    _phitr = 2.*M_PI + _phitr;
  }
  _thetatr = acos(cosThetaTr) ;
 
 
       
  }


};
#endif