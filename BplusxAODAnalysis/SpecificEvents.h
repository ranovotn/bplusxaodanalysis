#ifndef BsxAODAnalysis_SpecificEvents_H
#define BsxAODAnalysis_SpecificEvents_H

#include <string>
#include <vector>

#include <EventLoop/Algorithm.h>

#include <BPhysxAODTools/BranchVariable.h>


class TH1;
class TTree;
class GoodRunsListSelectionTool;
namespace Trig
  {
  class TrigDecisionTool;
  }
namespace TrigConf
  {
  class xAODConfigTool;
  }


class SpecificEvents : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  TString EventFile;
  std::map<uint32_t, std::vector<unsigned long long>> m_Events;
  std::map<uint32_t, std::vector<unsigned long long>>::iterator lastrun;//!
  TTree *m_tree; //!
  // this is a standard constructor
  SpecificEvents ();

    // Branches Group 2 branches are MC only
  ScalarBranchVariable<uint32_t          > m_run_number          = ScalarBranchVariable<uint32_t          >("run_number"          , this); //!
  ScalarBranchVariable<uint32_t          > m_lumi_block          = ScalarBranchVariable<uint32_t          >("lumi_block"          , this); //!
  ScalarBranchVariable<unsigned long long> m_evt_number          = ScalarBranchVariable<unsigned long long>("evt_number"          , this); //!

  std::vector<TString> m_Triggers; //!

  std::string              output_name;
  int m_event_counter; //!

  Trig::TrigDecisionTool    *m_trig_decision_tool; //!
  TrigConf::xAODConfigTool  *m_trig_config_tool  ; //!

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SpecificEvents, 1);
};

#endif
