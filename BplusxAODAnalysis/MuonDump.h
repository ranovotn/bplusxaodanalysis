#ifndef BsxAODAnalysis_MuonDump_H
#define BsxAODAnalysis_MuonDump_H

#include <EventLoop/Algorithm.h>
#include <BPhysxAODTools/BranchVariable.h>

class MuonDump : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

    std::string              output_name;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!


  ScalarBranchVariable<uint32_t          > m_run_number          = ScalarBranchVariable<uint32_t          >("run_number"          , this); //!
  ScalarBranchVariable<uint32_t          > m_lumi_block          = ScalarBranchVariable<uint32_t          >("lumi_block"          , this); //!
  ScalarBranchVariable<unsigned long long> m_evt_number          = ScalarBranchVariable<unsigned long long>("evt_number"          , this); //!


  ScalarBranchVariable<float> m_mu_pT            = ScalarBranchVariable<float>("mu_pT"  , this); //!
  ScalarBranchVariable<float> m_mu_eta            = ScalarBranchVariable<float>("mu_eta"  , this); //!
  ScalarBranchVariable<float> m_mu_phi            = ScalarBranchVariable<float>("mu_phi"  , this); //!
  ScalarBranchVariable<float> m_mu_charge            = ScalarBranchVariable<float>("mu_charge"  , this); //!
  ScalarBranchVariable<float> m_mu_phi0            = ScalarBranchVariable<float>("mu_phi0"  , this); //!
  ScalarBranchVariable<float> m_mu_d0            = ScalarBranchVariable<float>("mu_d0"  , this); //!
  ScalarBranchVariable<float> m_mu_z0            = ScalarBranchVariable<float>("mu_z0"  , this); //!
  ScalarBranchVariable<float> m_mu_theta            = ScalarBranchVariable<float>("mu_theta"  , this); //!
  ScalarBranchVariable<float> m_mu_qOverP            = ScalarBranchVariable<float>("mu_qOverP"  , this); //!
  ScalarBranchVariable<float> averageInteractionsPerCrossing            = ScalarBranchVariable<float>("averageInteractionsPerCrossing"  , this); //!
  ScalarBranchVariable<float> actualInteractionsPerCrossing            = ScalarBranchVariable<float>("actualInteractionsPerCrossing"  , this); //!
    // this is a standard constructor
  MuonDump ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MuonDump, 1);
  
private:
  TTree *m_tree;//!
};

#endif
