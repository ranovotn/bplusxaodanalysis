#ifndef BsxAODAnalysis_TriggerTest_H
#define BsxAODAnalysis_TriggerTest_H

#include <EventLoop/Algorithm.h>
#include <BPhysxAODTools/BranchVariable.h>


class TH1;
class TTree;
class GoodRunsListSelectionTool;
namespace Trig
  {
  class TrigDecisionTool;
  }
namespace TrigConf
  {
  class xAODConfigTool;
  }


class TriggerTest : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  std::string              output_name;
  std::string jpsi_container_name;
  TTree *m_tree_trigger; //!
  TTree *m_tree; //!
  size_t m_event_counter;//!
      std::string              print_trigger_regexp;//!
  size_t m_n_events_with_zero_jpsis;//!
  std::vector<ScalarBranchVariable<int> *> m_trigger_branches;//!
  std::vector<std::string> triggers;
  Trig::TrigDecisionTool    *m_trig_decision_tool; //!
  TrigConf::xAODConfigTool  *m_trig_config_tool  ; //!

    // Branches
  ScalarBranchVariable<uint32_t          > m_run_number          = ScalarBranchVariable<uint32_t          >("run_number"          , this); //!
  ScalarBranchVariable<uint32_t          > m_lumi_block          = ScalarBranchVariable<uint32_t          >("lumi_block"          , this); //!
  ScalarBranchVariable<unsigned long long> m_evt_number          = ScalarBranchVariable<unsigned long long>("evt_number"          , this); //!

/*
    VectorBranchVariable<unsigned char     > m_B_Jpsi_index        = VectorBranchVariable<unsigned char     >("B_Jpsi_index"        , this); //!
    VectorBranchVariable<float> m_B_Jpsi_mass         = VectorBranchVariable<float>("B_Jpsi_mass"         , this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mass     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mass"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_rapidity = VectorBranchVariableMapped<float, unsigned char>("Jpsi_rapidity", dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_pT       = VectorBranchVariableMapped<float, unsigned char>("Jpsi_pT"      , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_chi2     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_chi2"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
*/

  // this is a standard constructor
  TriggerTest ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  void InitBranches();
  void ClearBranches();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TriggerTest, 1);
};

#endif
