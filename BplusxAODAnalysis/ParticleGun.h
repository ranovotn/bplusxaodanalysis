#ifndef BsxAODAnalysis_ParticleGun_H
#define BsxAODAnalysis_ParticleGun_H

#include <EventLoop/Algorithm.h>
#include "TTree.h"
#include <vector>
#include <string>


class ParticleGun : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  std::string output_name;
  TTree *m_tree; //!
  std::vector<float> m_True_pT;//!
  std::vector<float> m_True_eta;//!
  std::vector<float> m_True_phi;//!
//  std::vector<float> m_True_d0;//!
//  std::vector<float> m_True_z0;//!
  std::vector<int> m_PDG;//!
  std::vector<float> m_qoverP;//!

  std::vector<float> m_Reco_pT;//!
  std::vector<float> m_Reco_pTErr;//!
  std::vector<float> m_Reco_eta;//!
  std::vector<float> m_Reco_phi;//!
  std::vector<float> m_Reco_d0;//!
  std::vector<float> m_Reco_d0Err;//!
  std::vector<float> m_Reco_z0;//!
  uint32_t        m_lumi_block;//!
  uint32_t        m_run_number;//!
  unsigned long long           m_evt_number;//!

  size_t m_event_counter;//!
  // this is a standard constructor
  ParticleGun ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(ParticleGun, 1);
};

#endif
