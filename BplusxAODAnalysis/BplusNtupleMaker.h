#ifndef BplusxAODAnalysis_BplusNtupleMaker_H
#define BplusxAODAnalysis_BplusNtupleMaker_H

#ifndef BPHYTAGGINGENABLED
#define BPHYTAGGINGENABLED 1
#endif
//#define OLDROOTCORE

#include <string>
#include <vector>

#include <EventLoop/Algorithm.h>

#include <BPhysxAODTools/BranchVariable.h>

#include <BplusxAODAnalysis/BaBarAngles.h>
#include <BPhysxAODTools/TruthNtuple.h>

#ifdef BPHYTAGGINGENABLED

#include <BChargeFlavourTagTool/IBphysFlavourTaggingTool.h>
#include "AsgTools/ToolHandleArray.h" 
#include "AsgTools/AnaToolHandle.h"
#ifndef OLDROOTCORE
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#else
#include "MuonSelectorTools/IMuonSelectionTool.h"
#endif
#endif

class TH1;
class TTree;
class GoodRunsListSelectionTool;
namespace Trig
  {
  class TrigDecisionTool;
  }
namespace TrigConf
  {
  class xAODConfigTool;
  }


class BplusNtupleMaker : public EL::Algorithm
  {
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:
    // variables that can be set up through the job options
    std::string              jpsi_container_name;
    std::string              bplus_container_name;
    std::string              output_name;
    std::vector<std::string> grl_files;
    std::vector<std::string> triggers;
    std::string              print_trigger_regexp;
    bool                     use_grl;
    bool                     m_truthOnly;
    bool                     m_truthTreeOnly;
    bool                     m_doL1TriggerTree;
    bool                     m_DisableAllCands;
    bool                     m_enableMuonTagging;
    bool                     m_useCalibArea;
    bool                     m_muonTree;
    // cuts
    float                    cut_chi2_ndof;
    float                    cut_kaon_pT;
    float                    cut_pion_pT;
    float                    cut_Bmass_upper;
    float                    cut_Bmass_lower;
    int JpsiCode; // MC15 uses 999443 for some reason, normally it is 443
    bool DisableTrigger;

    enum class TruthDecayType {
      Bplus_to_JPSI_Kplus,
      Bminus_to_JPSI_Kminus
    };
    TruthDecayType m_truthDecayType;

    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
    int m_event_counter; //!
    int m_trueFilled;//!
    int m_skip_grl     ; //!
    int m_skip_cuts    ; //!

    uint8_t GetTrackPropertyInt(const xAOD::TrackParticle *track, const xAOD::SummaryType e);
    // Histograms
    TH1 *m_h_jpsi_mass ; //!
    TH1 *m_h_jpsi_pT   ; //!
    TH1 *m_h_jpsi_rap  ; //!
    TH1 *m_h_bplus_mass; //!
    TH1 *m_h_bplus_pT  ; //!
    TH1 *m_h_bplus_rap ; //!

    // Trees
    TTree *m_tree; //!
    TTree *m_tree_single; //!
    TTree *m_tree_trigger; //!
    TTree *m_treeTruth; //!
    TTree *m_treeMuon; //!

    // tools
    GoodRunsListSelectionTool *m_grl               ; //!
    Trig::TrigDecisionTool    *m_trig_decision_tool; //!
    TrigConf::xAODConfigTool  *m_trig_config_tool  ; //!

    // Branches Group 2 branches are MC only
    ScalarBranchVariable<uint32_t          > m_run_number          = ScalarBranchVariable<uint32_t          >("run_number"          , this); //!
    ScalarBranchVariable<uint32_t          > m_lumi_block          = ScalarBranchVariable<uint32_t          >("lumi_block"          , this); //!
    ScalarBranchVariable<unsigned long long> m_evt_number          = ScalarBranchVariable<unsigned long long>("evt_number"          , this); //!

    ScalarBranchVariable<float> m_beamS_x = ScalarBranchVariable<float>("BeamPos_x"          , this); //!
    ScalarBranchVariable<float> m_beamS_y = ScalarBranchVariable<float>("BeamPos_y"          , this); //!
    ScalarBranchVariable<float> m_beamS_z = ScalarBranchVariable<float>("BeamPos_z"          , this); //!

    ScalarBranchVariable<float> m_beamSig_x = ScalarBranchVariable<float>("BeamPosSig_x"          , this); //!
    ScalarBranchVariable<float> m_beamSig_y = ScalarBranchVariable<float>("BeamPosSig_y"          , this); //!
    ScalarBranchVariable<float> m_beamSig_z = ScalarBranchVariable<float>("BeamPosSig_z"          , this); //!

    // GRL
    ScalarBranchVariable<bool > m_pass_GRL            = ScalarBranchVariable<bool >("pass_GRL"            , this); //!

    VectorBranchVariable<float> m_B_mass              = VectorBranchVariable<float>("B_mass"              , this); //!
    VectorBranchVariable<float> m_B_mass_err          = VectorBranchVariable<float>("B_mass_err"          , this); //!
    VectorBranchVariable<float> m_B_rapidity          = VectorBranchVariable<float>("B_rapidity"          , this); //!
    VectorBranchVariable<float> m_B_pT                = VectorBranchVariable<float>("B_pT"                , this); //!
    VectorBranchVariable<float> m_B_pT_err            = VectorBranchVariable<float>("B_pT_err"            , this); //!
    VectorBranchVariable<float> m_B_Lxy_MaxSumPt      = VectorBranchVariable<float>("B_Lxy_MaxSumPt"      , this); //!
    VectorBranchVariable<float> m_B_Lxy_MaxSumPt_err  = VectorBranchVariable<float>("B_Lxy_MaxSumPt_err"  , this); //!
    VectorBranchVariable<float> m_B_A0_MaxSumPt       = VectorBranchVariable<float>("B_A0_MaxSumPt"       , this); //!
    VectorBranchVariable<float> m_B_A0_MaxSumPt_err   = VectorBranchVariable<float>("B_A0_MaxSumPt_err"   , this); //!
    VectorBranchVariable<float> m_B_A0xy_MaxSumPt     = VectorBranchVariable<float>("B_A0xy_MaxSumPt"     , this); //!
    VectorBranchVariable<float> m_B_A0xy_MaxSumPt_err = VectorBranchVariable<float>("B_A0xy_MaxSumPt_err" , this); //!
    VectorBranchVariable<float> m_B_Z0_MaxSumPt       = VectorBranchVariable<float>("B_Z0_MaxSumPt"       , this); //!
    VectorBranchVariable<float> m_B_Z0_MaxSumPt_err   = VectorBranchVariable<float>("B_Z0_MaxSumPt_err"   , this); //!
    VectorBranchVariable<float> m_B_tau_MaxSumPt      = VectorBranchVariable<float>("B_tau_MaxSumPt"      , this); //!
    VectorBranchVariable<float> m_B_tau_MaxSumPt_err  = VectorBranchVariable<float>("B_tau_MaxSumPt_err"  , this); //!
    VectorBranchVariable<float> m_B_Lxy_MinA0         = VectorBranchVariable<float>("B_Lxy_MinA0"         , this); //!
    VectorBranchVariable<float> m_B_Lxy_MinA0_err     = VectorBranchVariable<float>("B_Lxy_MinA0_err"     , this); //!
    VectorBranchVariable<float> m_B_A0_MinA0          = VectorBranchVariable<float>("B_A0_MinA0"          , this); //!
    VectorBranchVariable<float> m_B_A0_MinA0_err      = VectorBranchVariable<float>("B_A0_MinA0_err"      , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinA0        = VectorBranchVariable<float>("B_A0xy_MinA0"        , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinA0_err    = VectorBranchVariable<float>("B_A0xy_MinA0_err"    , this); //!
    VectorBranchVariable<float> m_B_Z0_MinA0          = VectorBranchVariable<float>("B_Z0_MinA0"          , this); //!
    VectorBranchVariable<float> m_B_Z0_MinA0_err      = VectorBranchVariable<float>("B_Z0_MinA0_err"      , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0         = VectorBranchVariable<float>("B_tau_MinA0"         , this); //!
    VectorBranchVariable<float> m_B_tau_MinA0_err     = VectorBranchVariable<float>("B_tau_MinA0_err"     , this); //!
    VectorBranchVariable<float> m_B_Lxy_MinZ0         = VectorBranchVariable<float>("B_Lxy_MinZ0"         , this); //!
    VectorBranchVariable<float> m_B_Lxy_MinZ0_err     = VectorBranchVariable<float>("B_Lxy_MinZ0_err"     , this); //!
    VectorBranchVariable<float> m_B_A0_MinZ0          = VectorBranchVariable<float>("B_A0_MinZ0"          , this); //!
    VectorBranchVariable<float> m_B_A0_MinZ0_err      = VectorBranchVariable<float>("B_A0_MinZ0_err"      , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinZ0        = VectorBranchVariable<float>("B_A0xy_MinZ0"        , this); //!
    VectorBranchVariable<float> m_B_A0xy_MinZ0_err    = VectorBranchVariable<float>("B_A0xy_MinZ0_err"    , this); //!
    VectorBranchVariable<float> m_B_Z0_MinZ0          = VectorBranchVariable<float>("B_Z0_MinZ0"          , this); //!
    VectorBranchVariable<float> m_B_Z0_MinZ0_err      = VectorBranchVariable<float>("B_Z0_MinZ0_err"      , this); //!
    VectorBranchVariable<float> m_B_tau_MinZ0         = VectorBranchVariable<float>("B_tau_MinZ0"         , this); //!
    VectorBranchVariable<float> m_B_tau_MinZ0_err     = VectorBranchVariable<float>("B_tau_MinZ0_err"     , this); //!
    VectorBranchVariable<float> m_B_chi2_ndof         = VectorBranchVariable<float>("B_chi2_ndof"         , this); //!
    VectorBranchVariable<float> m_B_mu1_eta           = VectorBranchVariable<float>("B_mu1_eta"           , this); //!
    VectorBranchVariable<float> m_B_mu1_phi           = VectorBranchVariable<float>("B_mu1_phi"           , this); //!


    VectorBranchVariable<float> m_B_mu1_pT            = VectorBranchVariable<float>("B_mu1_pT"            , this); //!
    VectorBranchVariable<float> m_B_mu1_charge        = VectorBranchVariable<float>("B_mu1_charge"        , this); //!
    VectorBranchVariable<float> m_B_mu1_d0        = VectorBranchVariable<float>("B_mu1_d0"        , this); //!    
    VectorBranchVariable<float> m_B_mu1_extrapd0        = VectorBranchVariable<float>("B_mu1_extrapd0_minA0"        , this); //!
    VectorBranchVariable<float> m_B_mu2_eta           = VectorBranchVariable<float>("B_mu2_eta"           , this); //!
    VectorBranchVariable<float> m_B_mu2_phi           = VectorBranchVariable<float>("B_mu2_phi"           , this); //!
    VectorBranchVariable<float> m_B_mu2_pT            = VectorBranchVariable<float>("B_mu2_pT"            , this); //!
    VectorBranchVariable<float> m_B_mu2_charge        = VectorBranchVariable<float>("B_mu2_charge"        , this); //!
    VectorBranchVariable<float> m_B_mu2_d0        = VectorBranchVariable<float>("B_mu2_d0"        , this); //!    
    VectorBranchVariable<float> m_B_mu2_extrapd0        = VectorBranchVariable<float>("B_mu2_extrapd0_minA0"        , this); //!    
    VectorBranchVariable<float> m_B_trk1_eta          = VectorBranchVariable<float>("B_trk1_eta"          , this); //!
    VectorBranchVariable<float> m_B_trk1_phi          = VectorBranchVariable<float>("B_trk1_phi"          , this); //!
    

    VectorBranchVariable<float> m_B_trk1_pT           = VectorBranchVariable<float>("B_trk1_pT"           , this); //!
    VectorBranchVariable<float> m_B_trk1_charge       = VectorBranchVariable<float>("B_trk1_charge"       , this); //!
    VectorBranchVariable<float> m_B_trk1_d0        = VectorBranchVariable<float>("B_trk1_d0"        , this); //!    
    VectorBranchVariable<float> m_B_trk1_extrapd0        = VectorBranchVariable<float>("B_trk1_extrapd0_minA0"        , this); //!  
    VectorBranchVariable<unsigned char     > m_B_Jpsi_index        = VectorBranchVariable<unsigned char>("B_Jpsi_index", this); //!
    VectorBranchVariable<float> m_B_Jpsi_mass         = VectorBranchVariable<float>("B_Jpsi_mass"         , this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mass     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mass"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_rapidity = VectorBranchVariableMapped<float, unsigned char>("Jpsi_rapidity", dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_pT       = VectorBranchVariableMapped<float, unsigned char>("Jpsi_pT"      , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_chi2     = VectorBranchVariableMapped<float, unsigned char>("Jpsi_chi2"    , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu1_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu1_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_eta  = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_eta" , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariableMapped<float, unsigned char> m_Jpsi_mu2_pT   = VectorBranchVariableMapped<float, unsigned char>("Jpsi_mu2_pT"  , dynamic_cast<std::vector<unsigned char> *>(&this->m_B_Jpsi_index), this); //!
    VectorBranchVariable<int  > m_MaxSumPt_orig_ntrk  = VectorBranchVariable<int  >("MaxSumPt_orig_ntrk"  , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_ref_ntrk   = VectorBranchVariable<int  >("MaxSumPt_ref_ntrk"   , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_ref_stat   = VectorBranchVariable<int  >("MaxSumPt_ref_stat"   , this); //!
    VectorBranchVariable<int  > m_MinA0_orig_ntrk     = VectorBranchVariable<int  >("MinA0_orig_ntrk"     , this); //!
    VectorBranchVariable<int  > m_MinA0_ref_ntrk      = VectorBranchVariable<int  >("MinA0_ref_ntrk"      , this); //!
    VectorBranchVariable<int  > m_MinA0_ref_stat      = VectorBranchVariable<int  >("MinA0_ref_stat"      , this); //!
    VectorBranchVariable<int  > m_MinZ0_orig_ntrk     = VectorBranchVariable<int  >("MinZ0_orig_ntrk"     , this); //!
    VectorBranchVariable<int  > m_MinZ0_ref_ntrk      = VectorBranchVariable<int  >("MinZ0_ref_ntrk"      , this); //!
    VectorBranchVariable<int  > m_MinZ0_ref_stat      = VectorBranchVariable<int  >("MinZ0_ref_stat"      , this); //!
    VectorBranchVariable<int  > m_MaxSumPt_vertextype = VectorBranchVariable<int  >("MaxSumPt_vertextype" , this); //!
    VectorBranchVariable<int  > m_MinA0_vertextype    = VectorBranchVariable<int  >("MinA0_vertextype"    , this); //!
    VectorBranchVariable<int  > m_MinZ0_vertextype    = VectorBranchVariable<int  >("MinZ0_vertextype"    , this); //!

    VectorBranchVariable<float  > m_MaxSumPt_SumPt  = VectorBranchVariable<float  >("MaxSumPt_SumPt"  , this); //!
    VectorBranchVariable<float  > m_MaxSumPt_SumPtSq  = VectorBranchVariable<float  >("MaxSumPt_SumPtSq"  , this); //!
    VectorBranchVariable<float  > m_MinA0_SumPt  = VectorBranchVariable<float  >("MinA0_SumPt"  , this); //!
    VectorBranchVariable<float  > m_MinA0_SumPtSq  = VectorBranchVariable<float  >("MinA0_SumPtSq"  , this); //!
    VectorBranchVariable<float  > m_MinZ0_SumPt  = VectorBranchVariable<float  >("MinZ0_SumPt"  , this); //!
    VectorBranchVariable<float  > m_MinZ0_SumPtSq  = VectorBranchVariable<float  >("MinZ0_SumPtSq"  , this); //!

    VectorBranchVariable<float  > m_OrigMaxSumPt_SumPt  = VectorBranchVariable<float  >("Orig_MaxSumPt_SumPt"  , this); //!
    VectorBranchVariable<float  > m_OrigMaxSumPt_SumPtSq  = VectorBranchVariable<float  >("Orig_MaxSumPt_SumPtSq"  , this); //!
    VectorBranchVariable<float  > m_OrigMinA0_SumPt  = VectorBranchVariable<float  >("Orig_MinA0_SumPt"  , this); //!
    VectorBranchVariable<float  > m_OrigMinA0_SumPtSq  = VectorBranchVariable<float  >("Orig_MinA0_SumPtSq"  , this); //!
    VectorBranchVariable<float  > m_OrigMinZ0_SumPt  = VectorBranchVariable<float  >("Orig_MinZ0_SumPt"  , this); //!
    VectorBranchVariable<float  > m_OrigMinZ0_SumPtSq  = VectorBranchVariable<float  >("Orig_MinZ0_SumPtSq"  , this); //!

    
    VectorBranchVariable<int32_t> m_IBLhits      = VectorBranchVariable<int32_t       >("IBLhits"             , this); //!

    VectorBranchVariable<int32_t> m_SCThitsmu1      = VectorBranchVariable<int32_t       >("SCThitsmu1"             , this); //!

    VectorBranchVariable<int32_t> m_SCThitsmu2      = VectorBranchVariable<int32_t       >("SCThitsmu2"             , this); //!

    VectorBranchVariable<int32_t> m_SCThitshad1      = VectorBranchVariable<int32_t       >("SCThithad1"             , this); //!
    


    /*VectorBranchVariable<float> m_TRANS_phi          = VectorBranchVariable<float >("TRANS_phi"           , this); //!
    VectorBranchVariable<float> m_TRANS_costheta     = VectorBranchVariable<float >("TRANS_costheta"      , this); //!
    VectorBranchVariable<float> m_HELTRANS_costheta2 = VectorBranchVariable<float >("HELTRANS_costheta2"  , this); //!
    VectorBranchVariable<float> m_HEL_costheta1      = VectorBranchVariable<float >("HEL_costheta1"       , this); //!
    VectorBranchVariable<float> m_HEL_chi            = VectorBranchVariable<float >("HEL_chi"             , this); //!
*/
   VectorBranchVariable<int> m_had1_PDG            = VectorBranchVariable<int >("had1_PDG"             , this, 2); //!
   
   VectorBranchVariable<int> m_mu1_PDG            = VectorBranchVariable<int >("mu1_PDG"             , this, 2); //!
   VectorBranchVariable<int> m_mu2_PDG            = VectorBranchVariable<int >("mu2_PDG"             , this, 2); //!   
   


   VectorBranchVariable<int> m_muParent_PDG            = VectorBranchVariable<int >("muParent_PDG"  , this, 2); //!   
   VectorBranchVariable<int> m_hadParent_PDG            = VectorBranchVariable<int >("hadParent_PDG" , this, 2); //!   
   VectorBranchVariable<int> m_grandParent_PDG            = VectorBranchVariable<int >("grandParent_PDG", this, 2); //!  
   
   VectorBranchVariable<bool  > m_exclusiveTrueBplus    = VectorBranchVariable<bool  >("exclusiveTrueBplus"    , this); //!
   VectorBranchVariable<bool  > m_inclusiveTrueBplus    = VectorBranchVariable<bool  >("inclusiveTrueBplus"    , this); //!   
   VectorBranchVariable<float  > m_TrueLifetime    = VectorBranchVariable<float  >("Bplus_TrueLifetime"    , this, 2); //!   
   ScalarBranchVariable<bool> m_HLT_2mu4_bBmumuxv2_EFBMuMuXFex_type9   = ScalarBranchVariable<bool>("m_HLT_2mu4_bBmumuxv2_EFBMuMuXFex_type9"    , this); //!   


   VectorBranchVariable<std::vector<float>> m_mu1_coVar     = VectorBranchVariable<std::vector<float> >("mu1_coVar" , this); //!
   VectorBranchVariable<std::vector<float>> m_mu2_coVar     = VectorBranchVariable<std::vector<float> >("mu2_coVar" , this); //!
   VectorBranchVariable<std::vector<float>> m_had1_coVar    = VectorBranchVariable<std::vector<float> >("had1_coVar" , this); //!

   VectorBranchVariable<float> m_Jpsi_Lxy_MaxSumPt     = VectorBranchVariable<float >("Jpsi_Lxy_MaxSumPt" , this); //!
   VectorBranchVariable<float> m_Jpsi_Lxy_MaxSumPt_err = VectorBranchVariable<float >("Jpsi_Lxy_MaxSumPt_err" , this); //!

   VectorBranchVariable<float> m_PV_minA0_x     = VectorBranchVariable<float >("PV_minA0_x" , this); //!
   VectorBranchVariable<float> m_PV_minA0_y     = VectorBranchVariable<float >("PV_minA0_y" , this); //!
   VectorBranchVariable<float> m_PV_minA0_z     = VectorBranchVariable<float >("PV_minA0_z" , this); //!

   VectorBranchVariable<float> m_orig_PV_minA0_x     = VectorBranchVariable<float >("orig_PV_minA0_x" , this); //!
   VectorBranchVariable<float> m_orig_PV_minA0_y     = VectorBranchVariable<float >("orig_PV_minA0_y" , this); //!
   VectorBranchVariable<float> m_orig_PV_minA0_z     = VectorBranchVariable<float >("orig_PV_minA0_z" , this); //!

   VectorBranchVariable<float> m_SecVtx_x     = VectorBranchVariable<float >("SecVtx_x" , this); //!
   VectorBranchVariable<float> m_SecVtx_y     = VectorBranchVariable<float >("SecVtx_y" , this); //!
   VectorBranchVariable<float> m_SecVtx_z     = VectorBranchVariable<float >("SecVtx_z" , this); //!

   VectorBranchVariable<int> m_radiativeTracks     = VectorBranchVariable<int >("radiativeTracks" , this, 2); //!

   VectorBranchVariable<float> m_TRUE_PV_x     = VectorBranchVariable<float >("TRUE_PV_x" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_PV_y     = VectorBranchVariable<float >("TRUE_PV_y" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_PV_z     = VectorBranchVariable<float >("TRUE_PV_z" , this, 2); //!

   VectorBranchVariable<float> m_TRUE_SecVtx_x     = VectorBranchVariable<float >("TRUE_SecVtx_x" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_SecVtx_y     = VectorBranchVariable<float >("TRUE_SecVtx_y" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_SecVtx_z     = VectorBranchVariable<float >("TRUE_SecVtx_z" , this, 2); //!

   VectorBranchVariable<float> m_TRUE_MuonPlus_Pt     = VectorBranchVariable<float >("TRUE_MuonPlus_Pt" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_MuonMinus_Pt     = VectorBranchVariable<float >("TRUE_MuonMinus_Pt" , this, 2); //!
   VectorBranchVariable<float> m_TRUE_B_Pt     = VectorBranchVariable<float >("TRUE_B_Pt" , this, 2); //!
   
    VectorBranchVariable<float> m_T_TRANS_phi          = VectorBranchVariable<float >("TRUE_TRANS_phi"           , this, 2); //!
    VectorBranchVariable<float> m_T_TRANS_costheta     = VectorBranchVariable<float >("TRUE_TRANS_costheta"      , this, 2); //!
    VectorBranchVariable<float> m_T_HELTRANS_costheta2 = VectorBranchVariable<float >("TRUE_HELTRANS_costheta2"  , this, 2); //!
    VectorBranchVariable<float> m_T_HEL_costheta1      = VectorBranchVariable<float >("TRUE_HEL_costheta1"       , this, 2); //!
    VectorBranchVariable<float> m_T_HEL_chi            = VectorBranchVariable<float >("TRUE_HEL_chi"             , this, 2); //!   
   
   VectorBranchVariable<float> m_muonPt     = VectorBranchVariable<float >("muonPt"      , this, 5); //!
   VectorBranchVariable<float> m_muonEta    = VectorBranchVariable<float >("muonEta"      , this, 5); //!
   VectorBranchVariable<float> m_muonPhi    = VectorBranchVariable<float >("muonPhi"      , this, 5); //!
   VectorBranchVariable<float> m_muonRapity = VectorBranchVariable<float >("muonRapity"      , this, 5); //!
   VectorBranchVariable<float> m_D0    = VectorBranchVariable<float  >("D0"    , this, 5); //!
//   VectorBranchVariable<float> m_truthD0    = VectorBranchVariable<float  >("TrueD0"    , this, 5); //!
   VectorBranchVariable<float> m_TruemuonPt = VectorBranchVariable<float >("TruemuonPt"  , this, 5); //!
   VectorBranchVariable<int> m_TrueParentPDG    = VectorBranchVariable<int  >("TrueParentPDG"    , this, 5); //!


   ScalarBranchVariable<int> m_Npv   = ScalarBranchVariable<int>("nPV"    , this); //!
   ScalarBranchVariable<int> m_Ntrack = ScalarBranchVariable<int>("nTracks"    , this); //!
   ScalarBranchVariable<float> m_averageInteractionsPerCrossing = ScalarBranchVariable<float>("averageInteractionsPerCrossing"    , this); //!
   ScalarBranchVariable<float> m_actualInteractionsPerCrossing = ScalarBranchVariable<float>("actualInteractionsPerCrossing"    , this); //!
    std::vector<ScalarBranchVariable<int> *> m_trigger_branches;

    int m_n_B_candidates_loose_chi; //!
    int m_n_B_candidates_tight_chi; //!
    int m_n_Jpsi_candidates; //!
    int m_n_events_with_zero_jpsis; //!
    std::unique_ptr<xAOD::TruthNtupleNode> BplusDecayFinder;//!

#ifdef BPHYTAGGINGENABLED
    // Muon Tagging
    ToolHandleArray<IBphysFlavourTaggingTool> m_taggingTools; //!
#endif      
      
    // this is a standard constructor
    BplusNtupleMaker();

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob(EL::Job& job);
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode changeInput(bool firstFile);
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode postExecute();
    virtual EL::StatusCode finalize();
    virtual EL::StatusCode histFinalize();
#ifdef BPHYTAGGINGENABLED
    EL::StatusCode SetupMuonTagging();
#endif
    // 
    void InitBranches();
    void ClearBranches();
    int TrueDecaysFound;//!
    int TrueDecaysFoundOver1;//!    
    int FullReconFound;//!


#ifdef BPHYTAGGINGENABLED
#ifdef OLDROOTCORE
      asg::AnaToolHandle< CP::IMuonSelectionTool > m_muonSelTool_veryLoose; //!
      asg::AnaToolHandle< CP::IMuonSelectionTool > m_muonSelTool_loose;  //!
      asg::AnaToolHandle< CP::IMuonSelectionTool > m_muonSelTool_medium; //!
      asg::AnaToolHandle< CP::IMuonSelectionTool > m_muonSelTool_tight; //!
#else
     ToolHandle< CP::IMuonSelectionTool > m_muonSelTool_veryLoose; //!
     ToolHandle< CP::IMuonSelectionTool > m_muonSelTool_loose;  //!
     ToolHandle< CP::IMuonSelectionTool > m_muonSelTool_medium; //!
     ToolHandle< CP::IMuonSelectionTool > m_muonSelTool_tight; //!
#endif
#endif
      
    // this is needed to distribute the algorithm to the workers
    ClassDef(BplusNtupleMaker, 1);
    

private:
    TTree *m_L1Tree;    //!
    std::vector<TString> *m_L1Trig;//!
    std::vector<unsigned int> *m_passedbits;//!
    std::vector<bool> *m_passedbeforeprescale;//!
      

  };

#endif
