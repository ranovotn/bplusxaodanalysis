/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "xAODTrigBphys/TrigBphysContainer.h"
#include <BplusxAODAnalysis/BplusNtupleMaker.h>
#include "BPhysxAODTools/BPhysFunctions.h"
#include "TSystem.h"
#include <TH1.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>
#include <functional>
#include <unordered_set>
#include <algorithm>
#include "PathResolver/PathResolver.h"


// this is needed to distribute the algorithm to the workers
ClassImp(BplusNtupleMaker)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
        do {                                                     \
            if( ! EXP.isSuccess() ) {                             \
                Error( CONTEXT,                                    \
                        XAOD_MESSAGE( "Failed to execute: %s" ),    \
#EXP );                                     \
                return EL::StatusCode::FAILURE;                    \
            }                                                     \
        } while( false )

    typedef ElementLink<xAOD::VertexContainer> VertexLink;
    typedef std::vector<VertexLink> VertexLinkVector;


BplusNtupleMaker::BplusNtupleMaker()
    : jpsi_container_name ("BPHY5JpsiCandidates"),
    bplus_container_name("BPHY5BpmJpsiKpmCandidates"),
    output_name         ("DefaultOutput"),
    print_trigger_regexp(""),
    use_grl        (false),
    m_truthOnly (false),
    m_truthTreeOnly(false),
    m_doL1TriggerTree(false), m_DisableAllCands(false),m_enableMuonTagging(false),
    cut_chi2_ndof  (   2.5f),
    cut_kaon_pT    (1000.0f),
    cut_Bmass_upper    (5800.0f),
    cut_Bmass_lower    (4900.0f),
    m_event_counter(0),
    m_skip_grl     (0),
    m_skip_cuts    (0),
    m_h_jpsi_mass       (nullptr),
    m_h_jpsi_pT         (nullptr),
    m_h_jpsi_rap        (nullptr),
    m_h_bplus_mass         (nullptr),
    m_h_bplus_pT           (nullptr),
    m_h_bplus_rap          (nullptr),
    m_tree              (nullptr),
    m_tree_single       (nullptr),
    m_tree_trigger      (nullptr),
    m_treeMuon          (nullptr),
    m_grl               (nullptr),
    m_trig_decision_tool(nullptr),
    m_trig_config_tool  (nullptr),
    m_n_B_candidates_loose_chi(0),
    m_n_B_candidates_tight_chi(0),
    m_n_Jpsi_candidates(0),
    m_n_events_with_zero_jpsis(0),
                      BplusDecayFinder(nullptr),
    m_L1Tree(nullptr),m_L1Trig(nullptr)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
    JpsiCode = 443;
    DisableTrigger = false;
    m_truthDecayType = TruthDecayType::Bplus_to_JPSI_Kplus;
    m_useCalibArea = false;
    m_muonTree = false;
}



EL::StatusCode BplusNtupleMaker::setupJob(EL::Job& job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.

    job.useXAOD();
    EL_RETURN_CHECK("setupJob()", xAOD::Init());

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode BplusNtupleMaker::histInitialize()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    m_h_jpsi_mass  = new TH1D("h_JpsiMass" , "h_JpsiMass" , 2000,  0.0, 20000.0);
    m_h_jpsi_pT    = new TH1D("h_JpsiPt"   , "h_JpsiPt"   , 2000,  0.0, 60000.0);
    m_h_jpsi_rap   = new TH1D("h_JpsiRap"  , "h_JpsiRap"  , 2000, -2.6, 2.6    );
    m_h_bplus_mass = new TH1D("h_BplusMass", "h_BplusMass", 2000,  0.0, 20000.0);
    m_h_bplus_pT   = new TH1D("h_BplusPt"  , "h_BplusPt"  , 2000,  0.0, 60000.0);
    m_h_bplus_rap  = new TH1D("h_BplusRap" , "h_BplusRap" , 2000, -2.6, 2.6    );

    wk()->addOutput(m_h_jpsi_mass);
    wk()->addOutput(m_h_jpsi_pT  );
    wk()->addOutput(m_h_jpsi_rap );
    wk()->addOutput(m_h_bplus_mass);
    wk()->addOutput(m_h_bplus_pT  );
    wk()->addOutput(m_h_bplus_rap );


    TFile *output_file = wk()->getOutputFile(output_name);
    m_tree = nullptr;
    if(!m_DisableAllCands)  m_tree         = new TTree("BplusAllCandidates", "BplusAllCandidates");
    m_tree_single  =   new TTree("BplusBestChi"      , "BplusBestChi"      );
    m_tree_trigger =  !DisableTrigger ?  new TTree("BplusTriggers"    , "BplusTriggers"     ) : nullptr;
    if(!m_DisableAllCands)  m_tree       ->SetDirectory(output_file);
    m_tree_single ->SetDirectory(output_file);
    if(m_tree_trigger) m_tree_trigger->SetDirectory(output_file);

    m_treeMuon = m_muonTree ? new TTree("muonTree", "muonTree") : nullptr;
    if(m_muonTree) m_treeMuon->SetDirectory(output_file);

    if(m_doL1TriggerTree){
        m_L1Tree = new TTree("L1TrigTree", "L1TrigTree");
        m_L1Tree->SetDirectory(output_file);
        m_L1Trig = new std::vector<TString>();
        m_passedbits = new std::vector<unsigned int>();
        m_passedbeforeprescale  = new std::vector<bool>();
        m_L1Tree->Branch("TrigNames", &m_L1Trig);
        m_L1Tree->Branch("passedBits", &m_passedbits);
        m_L1Tree->Branch("passedBeforePrescale", &m_passedbeforeprescale);

    }

    InitBranches();


    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusNtupleMaker::fileExecute()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusNtupleMaker::changeInput(bool /*firstFile*/)
{
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
}

int ParentPDG(const xAOD::TruthParticle* tr1, const xAOD::TruthParticle* tr2){
    if(tr1 == tr2 && tr1) return tr1->pdgId();
    return 0;
}

EL::StatusCode BplusNtupleMaker::initialize()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.

    xAOD::TEvent* event = wk()->xaodEvent();
    Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

    m_event_counter = 0;
    m_skip_grl      = 0;
    m_skip_cuts     = 0;

    if (use_grl)
    {
        Info("initialize()", "use_grl = true (GRLs will be applied for data)");
        for(auto &str : grl_files){
            if(!m_useCalibArea) str = gSystem->ExpandPathName (str.c_str());
            else                str = PathResolverFindCalibFile(str);
            Info("initialize()", str.c_str());
        }
        m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
        EL_RETURN_CHECK("initialize()", m_grl->setProperty("GoodRunsListVec", grl_files));
        EL_RETURN_CHECK("initialize()", m_grl->setProperty("PassThrough", false)); // if true (default) will ignore result of GRL and will just pass all events
        EL_RETURN_CHECK("initialize()", m_grl->initialize());
    }
    else
        Info("initialize()", "use_grl = false (GRLs will not be applied)"); 

    if(!m_truthTreeOnly && !DisableTrigger){
        m_trig_config_tool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
        EL_RETURN_CHECK("initialize()", m_trig_config_tool->initialize());
        ToolHandle<TrigConf::ITrigConfigTool> trig_config_handle(m_trig_config_tool);
        m_trig_decision_tool = new Trig::TrigDecisionTool("TrigDecisionTool");
        EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("ConfigTool", trig_config_handle)); // connect the TrigDecisionTool to the ConfigTool
        EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("TrigDecisionKey", "xTrigDecision"));
        EL_RETURN_CHECK("initialize()", m_trig_decision_tool->initialize());
    }
    TrueDecaysFound =0;
    TrueDecaysFoundOver1 =0;
    FullReconFound =0;


    return EL::StatusCode::SUCCESS;
}

std::vector<const xAOD::TrackParticle*>  selectTracksInCone(
        const std::vector< const xAOD::TrackParticle*> & allTracks,
        TVector3            & axis,
        double deltaR,
        const std::vector<const xAOD::TrackParticle*> & excludedTracks)      {

    std::vector<const xAOD::TrackParticle*> selectedTracks;

    for (auto track : allTracks) {
        if (!track) {
            // off             ATH_MSG_WARNING("Null track in selectTracksInCone input");
        }
        if ( find(excludedTracks.begin(), excludedTracks.end(), track) != excludedTracks.end() ) continue;
        TVector3     ptl( track->p4().Px(), track->p4().Py(), track->p4().Pz());

        double deta = ptl.Eta() - axis.Eta();
        double dphi = ptl.Phi() - axis.Phi();
        double PI = M_PI;
        if(dphi > 2.*PI) { dphi = dphi - 2.*PI; }
        if(dphi <-2.*PI) { dphi = dphi + 2.*PI; }

        double dR   = sqrt( deta*deta + dphi*dphi);
//        std::cout << " dR deltaR " << dR << " " << deltaR << std::endl;
        if (dR > deltaR) continue; // remove the selected track
        // if here, track passed all selection requirements
        selectedTracks.push_back(track);

    } // for

    return selectedTracks;
}

TLorentzVector GetTLorentzVector(const xAOD::TruthParticle* p){
    TLorentzVector vect;
    vect.SetXYZM(p->px(), p->py(), p->pz(), p->m());
    return vect;
}

uint8_t BplusNtupleMaker::GetTrackPropertyInt(const xAOD::TrackParticle *track, const xAOD::SummaryType e) {
    uint8_t num=0;
    if (track->summaryValue(num, e)){
        return num;    
    }else
    { Error("GetTrackPropertyInt", "Track variable not failed"); return -1; }
}

void PrintTruth(const xAOD::TruthParticle *t){
    Info("execute()","Particle %i", t->pdgId());
    size_t children= t->nChildren();
    for(size_t i=0;i<children;i++){
        PrintTruth(t->child(i));
    }
    
      /* for(auto tru : *xTruthParticleContainer){
       Info("execute()", "particle %i ", tru->pdgId());
       size_t children= tru->nChildren();
       for(size_t i=0;i<children;i++){
       Info("execute()", "   child %i ", tru->child(i)->pdgId());
       }
       }*/
           
    //tru->child(i)->pdgId()
}

EL::StatusCode BplusNtupleMaker::execute()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    // make vector for sanity checking the candidates
    std::vector<const xAOD::Vertex*> found_tagging_bcandidates;
    // make a map that links the element in the container, to the index
    // of the candidates that pass the selections.
    // This is needed to remake the ElementLink of the object.
    std::map<unsigned int, unsigned int> map_allIndices_to_selected;

    xAOD::TEvent *event = wk()->xaodEvent();


    if ((m_event_counter % 100) == 0)
        Info("execute()", "%i events prcessed so far", m_event_counter);

    m_event_counter++;

    const xAOD::EventInfo *event_info = nullptr;
    EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));

    bool is_mc = event_info->eventType(xAOD::EventInfo::IS_SIMULATION);

    // Apply GRL in case it's data
    bool passGRL = true; // new
    if (!is_mc && use_grl && m_grl)
    {
        if (!m_grl->passRunLB(*event_info))
        {
            m_skip_grl++;
            // return EL::StatusCode::SUCCESS; // ! new
            passGRL = false; // new
        }
    }
    m_pass_GRL   = passGRL;
   
    m_evt_number = event_info->eventNumber();
    m_lumi_block = event_info->lumiBlock  ();
    m_run_number = event_info->runNumber  ();

    m_beamS_x = event_info->beamPosX ();
    m_beamS_y = event_info->beamPosY ();
    m_beamS_z = event_info->beamPosZ ();

    m_beamSig_x = event_info->beamPosSigmaX ();
    m_beamSig_y = event_info->beamPosSigmaY ();
    m_beamSig_z = event_info->beamPosSigmaZ ();  
    
    if(BplusDecayFinder == nullptr && is_mc){
        if (m_truthDecayType == TruthDecayType::Bplus_to_JPSI_Kplus) {
            BplusDecayFinder = std::unique_ptr<xAOD::TruthNtupleNode>( new xAOD::TruthNtupleNode(521, "Bplus") );
            auto *jpsi = BplusDecayFinder->addChild(JpsiCode, "Jpsi"); // MC15 uses 999443 for some reason, normally it is 443
            jpsi->addChild(-13, "Muplus");
            jpsi->addChild(13, "Muminus");
            BplusDecayFinder->addChild(321, "Kplus");
        } else {
            Error("execute()", "Unsupported m_truthDecayType value");
            return EL::StatusCode::FAILURE;
        }  
        BplusDecayFinder->print();

        m_treeTruth    = new TTree("BplusTruth"     , "BplusTruth"     );
        m_treeTruth->SetDirectory(wk()->getOutputFile(output_name));

        BplusDecayFinder->Book(m_treeTruth, &m_run_number.value,
                &m_lumi_block.value, &m_evt_number.value);

        //MC branches
        if(m_tree || m_tree_single) IBranchVariable::SetAllBranches(m_tree,  m_tree_single, this, 2);
    }

    std::vector<int> MatchedToFinderIndices;
    std::vector<std::array<const xAOD::Track_Particle*, 3>> TracksMatchtoTrue;
    std::vector<int> EndTracksThatExtraRadiate;
    std::vector<bool> Bexclusive;
    std::vector<const xAOD::Truth_Particle*> TrueBplusReconstructed;
    size_t truthsfound =0;

    if(is_mc){
        const xAOD::TruthParticleContainer* xTruthParticleContainer = nullptr;

        if(!event->retrieve( xTruthParticleContainer, "TruthParticles").isSuccess()) {
            Error("execute()", "Failed to find TruthParticles");
            return EL::StatusCode::FAILURE;
        }


        truthsfound = BplusDecayFinder->FindTruth(xTruthParticleContainer, true, false);
        //        if(truthsfound==0) BplusDecayFinder->printResults();

        TrueDecaysFound+=truthsfound;
        TrueDecaysFoundOver1+=truthsfound>1;
        BplusDecayFinder->PrepareForFill();
        m_treeTruth->Fill();      
        if(m_truthTreeOnly)   return EL::StatusCode::SUCCESS;
        const xAOD::TrackParticleContainer* tracks = 0;
        if(!event->retrieve( tracks, "InDetTrackParticles" ).isSuccess()) {
            Error("execute()", "Failed to find InDetTrackParticles");
            return EL::StatusCode::FAILURE;
        }

        for(size_t i=0;i<truthsfound;i++){
            xAOD::TruthParticleNode *jpsi = nullptr;
            const xAOD::Track_Particle* mu1    = nullptr;
            const xAOD::Track_Particle* mu2    = nullptr;
            const xAOD::Track_Particle* Kplus  = nullptr;
 
            if (m_truthDecayType == TruthDecayType::Bplus_to_JPSI_Kplus) {
              jpsi = BplusDecayFinder->getChildNode(0);
              mu1    = jpsi->getChildNode(0)->getBestTrack(tracks, i);
              mu2    = jpsi->getChildNode(1)->getBestTrack(tracks, i);
              Kplus  = BplusDecayFinder->getChildNode(1)->getBestTrack(tracks, i);
            } else {
              Error("execute()", "Unsupported m_truthDecayType value");
              return EL::StatusCode::FAILURE;
            }
            if(mu1 && mu2 && Kplus)
            {
                FullReconFound++;
                MatchedToFinderIndices.push_back(i);
                TracksMatchtoTrue.emplace_back();
                TracksMatchtoTrue.back() =  {mu1, mu2, Kplus};

                int TracksRadiate = 0;
                if(!jpsi->getChildNode(0)->DecaySegmentWasExclusive(i)) TracksRadiate |= 1 << 0;
                if(!jpsi->getChildNode(1)->DecaySegmentWasExclusive(i)) TracksRadiate |= 1 << 1;
                if(m_truthDecayType == TruthDecayType::Bplus_to_JPSI_Kplus) {
                  if(!BplusDecayFinder->getChildNode(1)->DecaySegmentWasExclusive(i))  TracksRadiate |= 1 << 2;
                } else {
                  Error("execute()", "Unsupported m_truthDecayType value");
                  return EL::StatusCode::FAILURE;
                }
                EndTracksThatExtraRadiate.push_back(TracksRadiate);

                bool exclusive = BplusDecayFinder->DecaySegmentWasExclusive(i) && jpsi->DecaySegmentWasExclusive(i);

                Bexclusive.push_back(exclusive);
                TrueBplusReconstructed.push_back(BplusDecayFinder->getTrueParticle(i));
            }
        }
    }





    const xAOD::VertexContainer *container_jpsi = nullptr;
    EL_RETURN_CHECK("execute()", event->retrieve(container_jpsi, jpsi_container_name.c_str()));
    if (container_jpsi->empty())
        m_n_events_with_zero_jpsis++;
    const xAOD::VertexContainer *container_bplus = nullptr;
    EL_RETURN_CHECK("execute()", event->retrieve(container_bplus, bplus_container_name.c_str()));
    if (container_bplus->empty() && !m_treeMuon)
    {
        ClearBranches();
        return EL::StatusCode::SUCCESS;
    }

    
    std::vector<const xAOD::Vertex *> good_jpsis;
    for (auto jpsi_cand : (*container_jpsi))
    {
        const xAOD::Vertex *jpsi_vertex = (const xAOD::Vertex *)jpsi_cand;
        xAOD::BPhysHelper jpsi_helper(jpsi_cand);
        if(jpsi_helper.nRefTrks() != 2 ||
                jpsi_helper.nMuons()   != 2)
        {
            Warning("execute()", "Expected two muon tracks, skip");
            continue;
        }
        m_n_Jpsi_candidates++;
        good_jpsis.push_back(jpsi_cand);
        TLorentzVector muon0_ref_track = jpsi_helper.refTrk(0, 105.65837);
        TLorentzVector muon1_ref_track = jpsi_helper.refTrk(1, 105.65837);
        TLorentzVector jpsi_ref_track = muon0_ref_track + muon1_ref_track;

        m_h_jpsi_pT ->Fill(jpsi_ref_track.Pt      ());
        m_h_jpsi_rap->Fill(jpsi_ref_track.Rapidity());

        float jpsi_mass = jpsi_vertex->auxdata<float>("Jpsi_mass");
        m_h_jpsi_mass->Fill(jpsi_mass);
        m_Jpsi_mass    .push_back(jpsi_mass);
        m_Jpsi_rapidity.push_back(jpsi_ref_track.Rapidity());
        m_Jpsi_pT      .push_back(jpsi_ref_track.Pt      ());
        m_Jpsi_chi2    .push_back(jpsi_helper.vtx()->chiSquared());
        m_Jpsi_mu1_eta .push_back(muon0_ref_track.Eta());
        m_Jpsi_mu1_pT  .push_back(muon0_ref_track.Pt ());
        m_Jpsi_mu2_eta .push_back(muon1_ref_track.Eta());
        m_Jpsi_mu2_pT  .push_back(muon1_ref_track.Pt ());
    }

    
    using namespace xAOD;
    static SG::AuxElement::Accessor< int > PV_count("OriginalCount_PrimaryVertices");
    static SG::AuxElement::Accessor< int > Track_count("OriginalCount_InDetTrackParticles");
    m_Npv =  PV_count.isAvailable(*event_info) ?  PV_count(*event_info) : 0;
    m_Ntrack =  Track_count.isAvailable(*event_info) ?  Track_count(*event_info) : 0;
    m_averageInteractionsPerCrossing = event_info->averageInteractionsPerCrossing();
    m_actualInteractionsPerCrossing  = event_info->actualInteractionsPerCrossing();

    const xAOD::MuonContainer *muons(nullptr);
    if (event->retrieve( muons, "Muons").isFailure()) {
        return EL::StatusCode::FAILURE;
    }
    
//Not used normally, used in special muon analysis
    if(m_treeMuon){
      bool ptpassed = false;
      for(auto muon : *muons){
          if(muon->pt() > 20000) ptpassed = true;
          m_muonPt.push_back(muon->pt());
          m_muonEta.push_back(muon->eta());
          m_muonPhi.push_back(muon->phi());
          m_muonRapity.push_back(muon->rapidity());
          auto trackpart = muon->primaryTrackParticle();
          m_D0.push_back(trackpart->d0());
        
          if(is_mc){

             auto truth =  BPhysFunctions::getTrkGenParticle(trackpart);
             if(truth){
               m_TruemuonPt.push_back(truth->pt());
               auto truthp = BPhysFunctions::getParentParticle(truth);
               m_TrueParentPDG.push_back(truthp ? truthp->pdgId() : 0);
//               if(TrueBplusReconstructed.empty()) m_truthD0.push_back( BPhysFunctions::PseudoD0(truth) );
//               else m_truthD0.push_back(BPhysFunctions::PseudoD0(truth, TrueBplusReconstructed[0]->prodVtx ()));
             }else{
               m_TruemuonPt.push_back(-1); m_TrueParentPDG.push_back(0); //m_truthD0.push_back(-999999);
             }
          }
       }
      if(!ptpassed)
      {
         ClearBranches();
         return EL::StatusCode::SUCCESS;
      }else{
        if(m_treeMuon) m_treeMuon->Fill();
        ClearBranches();
        return EL::StatusCode::SUCCESS;
      }
    }
///////
    int bplus_cand_counter(-1);
    int bplus_passes_counter(0);

    for (const xAOD::Vertex* bplus_cand : (*container_bplus))
    {
        ++bplus_cand_counter;

        xAOD::BPhysHypoHelper bplus_helper("Bplus", bplus_cand);
        if (bplus_helper.nRefTrks() != 3)
        {
            Warning("execute()", "Expected three (not %d) tracks for B candidate, skip", bplus_helper.nRefTrks());
            continue;
        }
        VertexLinkVector prec_vtx = bplus_cand->auxdata<VertexLinkVector>("PrecedingVertexLinks");

        if (prec_vtx.size() != 1)
        {
            Warning("execute()", "Expected one (not %d) preceding vertices for B candidate, skip", (int)prec_vtx.size());
            continue;
        }
        auto jpsi_it = std::find(good_jpsis.begin(), good_jpsis.end(), *(prec_vtx[0]));
        if (jpsi_it == good_jpsis.end())
        {
            Warning("execute()", "Unknown preceding vertex for B candidate, skip");
            continue;
        }

        m_n_B_candidates_loose_chi++;

        TLorentzVector muon0_ref_track = bplus_helper.refTrk(0, 105.65837);
        TLorentzVector muon1_ref_track = bplus_helper.refTrk(1, 105.65837);
        TLorentzVector kaon_ref_track = bplus_helper.refTrk(2, 493.677);

        TLorentzVector kaon_track = bplus_helper.refTrkOriginP(2, 493.677);


        TLorentzVector bplus_ref_track = muon0_ref_track + muon1_ref_track + kaon_ref_track;
        float chi2_ndof = bplus_helper.vtx()->chiSquared() / bplus_helper.vtx()->numberDoF();

        TLorentzVector jpsi_refitted = muon0_ref_track + muon1_ref_track;
	
        if (chi2_ndof > cut_chi2_ndof || kaon_ref_track.Pt () < cut_kaon_pT)
      	{
      		m_skip_cuts++;
      		continue;
      	}
        if ( bplus_helper.mass() < cut_Bmass_lower || cut_Bmass_upper < bplus_helper.mass())
      	{
      		m_skip_cuts++;
      		continue;
      	}

        std::array<const xAOD::TrackParticle*, 3> trackptr;
        std::array<int32_t, 3> sctHits;

        int32_t IBLhits= 0;
        for(size_t i =0; i<3;i++){
            trackptr[i] =  dynamic_cast<const xAOD::TrackParticle*>( bplus_helper.refTrkOrigin(i) );
            IBLhits += (GetTrackPropertyInt(trackptr[i], xAOD::SummaryType::numberOfInnermostPixelLayerHits) > 0);
            sctHits[i] = GetTrackPropertyInt(trackptr[i], xAOD::SummaryType::numberOfSCTHits);
        }

        if(is_mc){
            bool match = false;
            bool exclusive = false;
            size_t i;
            for(i =0; i<TracksMatchtoTrue.size(); i++){
                match = collectionsMatch(TracksMatchtoTrue[i], trackptr);
                if(match){
                    exclusive = Bexclusive[i];
                    break;
                }
            }
            if(m_truthOnly && !match) continue;///Skip non true cands if set
            m_exclusiveTrueBplus.push_back(match & exclusive);
            m_inclusiveTrueBplus.push_back(match);
            
  	    float lifetime = match ? BPhysFunctions::tau(TrueBplusReconstructed.at(i)): -1;
            m_TrueLifetime.push_back(lifetime);

            m_radiativeTracks.push_back(match ? EndTracksThatExtraRadiate.at(i) : -1);

            m_TRUE_PV_x.push_back( match ?  TrueBplusReconstructed[i]->prodVtx () ->x () : -999);
            m_TRUE_PV_y.push_back( match ?  TrueBplusReconstructed[i]->prodVtx () ->y () : -999);
            m_TRUE_PV_z.push_back( match ?  TrueBplusReconstructed[i]->prodVtx () ->z () : -999);

            m_TRUE_SecVtx_x.push_back( match ? TrueBplusReconstructed[i]->decayVtx() ->x () : -999);
            m_TRUE_SecVtx_y.push_back( match ? TrueBplusReconstructed[i]->decayVtx() ->y () : -999);
            m_TRUE_SecVtx_z.push_back( match ? TrueBplusReconstructed[i]->decayVtx() ->z () : -999);

            m_TRUE_MuonPlus_Pt.push_back(-999);
            m_TRUE_MuonMinus_Pt.push_back(-999);
            m_TRUE_B_Pt.push_back(-999);

            m_T_TRANS_phi.push_back(-999);
            m_T_TRANS_costheta.push_back(-999);
            m_T_HELTRANS_costheta2.push_back(-999);
            m_T_HEL_costheta1.push_back(-999);
            m_T_HEL_chi.push_back(-999);

            if(match){
                std::unique_ptr<BaBarAngles> BplusAngles;
                auto jpsi = BplusDecayFinder->getChildNode(0);
                auto mu1 = jpsi->getChildNode(0)->getTrueParticle(MatchedToFinderIndices.at(i));
                auto mu2 = jpsi->getChildNode(1)->getTrueParticle(MatchedToFinderIndices.at(i));
                if (m_truthDecayType == TruthDecayType::Bplus_to_JPSI_Kplus) {
                  auto Kplus = BplusDecayFinder->getChildNode(1)->getTrueParticle(MatchedToFinderIndices.at(i));
/*                  BplusAngles = std::unique_ptr<BaBarAngles>(new BaBarAngles(GetTLorentzVector(Kplus), GetTLorentzVector(mu1),
                                                                          GetTLorentzVector(phi->getTrueParticle(MatchedToFinderIndices.at(i))),
                                                                          GetTLorentzVector(jpsi->getTrueParticle(MatchedToFinderIndices.at(i))),
                                                                          GetTLorentzVector(BplusDecayFinder->getTrueParticle(MatchedToFinderIndices.at(i)))));*/
                } else {
                  Error("execute()", "Unsupported m_truthDecayType value");
                  return EL::StatusCode::FAILURE;
                }

                m_TRUE_MuonPlus_Pt.back() = mu1->pt();
                m_TRUE_MuonMinus_Pt.back() = mu2->pt();
                m_TRUE_B_Pt.back() = BplusDecayFinder->getTrueParticle(MatchedToFinderIndices.at(i))->pt();

                //m_T_TRANS_phi.back()     = (BplusAngles->phitrfix());
                //m_T_TRANS_costheta.back()= (BplusAngles->thetatrfix());
                //m_T_HELTRANS_costheta2.back() = (BplusAngles->theta1fix()); // ATLAS costheta1 = muon angle, Babar is it the kaon angle
                //m_T_HEL_costheta1.back() = (BplusAngles->theta2fix());      // ATLAS costheta1 = muon angle, Babar is it the kaon angle
                //m_T_HEL_chi.back()       = (BplusAngles->chifix());
            }


            std::array<const xAOD::TruthParticle*, 9> truthptr;
            auto getparent = [](const xAOD::TruthParticle* i) { return i ? i->parent() : nullptr; };
            for(size_t i =0; i<3;i++){
                truthptr.at((i*3)+0) =  BPhysFunctions::getTrkGenParticle(trackptr[i]);
                truthptr.at((i*3)+1) =  getparent(truthptr.at((i*3)+0));
                truthptr.at((i*3)+2) =  getparent(truthptr.at((i*3)+1));
            }
            auto getID = [](const xAOD::TruthParticle* i) { return i ? i->pdgId() : 0; };

            m_mu1_PDG .push_back(getID (truthptr.at((0*3)+0)));
            m_mu2_PDG .push_back(getID (truthptr.at((1*3)+0)));
            m_had1_PDG.push_back(getID (truthptr.at((2*3)+0)));

            int muparent =  ParentPDG(truthptr[(0*3)+1], truthptr[(1*3)+1]);
            //(truthptr[(0*3)+1] && truthptr[(0*3)+1] == truthptr[(1*3)+1]) ? truthptr[(0*3)+1]->pdgId() : 0;
            int Kparent = ParentPDG(truthptr[(2*3)+1],truthptr[(2*3)+1]);
            // (truthptr[(2*3)+1] && truthptr[(3*3)+1] == truthptr[(2*3)+1]) ? truthptr[(3*3)+1]->pdgId() : 0;


            bool grandparentmatch = true;
            if (m_truthDecayType == TruthDecayType::Bplus_to_JPSI_Kplus) {
              for(size_t i =1; i<3;i++)  grandparentmatch &= truthptr[(i*3)+2] == truthptr[((i-1)*3)+2];
            } else {
              Error("execute()", "Unsupported m_truthDecayType value");
              return EL::StatusCode::FAILURE;
            }
            int grandparent = (grandparentmatch && truthptr[(0*3)+2]) ? truthptr[(0*3)+2]->pdgId() : 0;

            m_muParent_PDG.push_back(muparent);
            m_hadParent_PDG.push_back(Kparent);

            m_grandParent_PDG.push_back(grandparent);

        }
        else{
            m_exclusiveTrueBplus.push_back(false);
            m_inclusiveTrueBplus.push_back(false);
        }


        
	m_IBLhits.push_back(IBLhits);
        m_SCThitsmu1.push_back(sctHits[0]);
        m_SCThitsmu2.push_back(sctHits[1]);
        m_SCThitshad1.push_back(sctHits[2]);



        /*BaBarAngles BplusAngles = BaBarAngles(kaon_ref_track, muon0_ref_track, Kstar_refitted, jpsi_refitted, bplus_ref_track);

        m_TRANS_phi.push_back(BplusAngles.phitrfix());
        m_TRANS_costheta.push_back(BplusAngles.thetatrfix());
        m_HELTRANS_costheta2.push_back(BplusAngles.theta1fix()); // ATLAS costheta1 = muon angle, Babar is it the kaon angle
        m_HEL_costheta1.push_back(BplusAngles.theta2fix());      // ATLAS costheta1 = muon angle, Babar is it the kaon angle
        m_HEL_chi.push_back(BplusAngles.chifix());
*/
        m_B_Jpsi_index.push_back(std::distance(good_jpsis.begin(), jpsi_it));
        m_B_Jpsi_mass .push_back((*jpsi_it)->auxdata<float>("Jpsi_mass"));

        m_B_chi2_ndof.push_back(chi2_ndof);
        m_n_B_candidates_tight_chi++;


        m_B_mu1_eta.push_back(muon0_ref_track.Eta());
        m_B_mu2_eta.push_back(muon1_ref_track.Eta());
        m_B_trk1_eta.push_back(kaon_ref_track .Eta());

        m_B_mu1_pT .push_back(muon0_ref_track.Pt ());
        m_B_mu2_pT .push_back(muon1_ref_track.Pt ());
        m_B_trk1_pT .push_back(kaon_ref_track .Pt ());


        m_B_mu1_phi.push_back(muon0_ref_track.Phi());
        m_B_mu2_phi.push_back(muon1_ref_track.Phi());
        m_B_trk1_phi.push_back(kaon_ref_track.Phi());



        m_B_rapidity.push_back(bplus_ref_track.Rapidity());
        m_B_pT      .push_back(bplus_ref_track.Pt      ());
        m_B_pT_err  .push_back(bplus_cand->auxdata<float>("PtErr"));

        xAOD::BPhysHelper jpsi_helper(*jpsi_it);

        m_Jpsi_Lxy_MaxSumPt    .push_back(jpsi_helper.lxy (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_Jpsi_Lxy_MaxSumPt_err.push_back(jpsi_helper.lxyErr (xAOD::BPhysHelper::PV_MAX_SUM_PT2));

        m_B_Lxy_MaxSumPt     .push_back(bplus_helper.lxy    (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_Lxy_MaxSumPt_err .push_back(bplus_helper.lxyErr (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_A0_MaxSumPt      .push_back(bplus_helper.a0     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_A0_MaxSumPt_err  .push_back(bplus_helper.a0Err  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_A0xy_MaxSumPt    .push_back(bplus_helper.a0xy   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_A0xy_MaxSumPt_err.push_back(bplus_helper.a0xyErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_Z0_MaxSumPt      .push_back(bplus_helper.z0     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_Z0_MaxSumPt_err  .push_back(bplus_helper.z0Err  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));

        m_B_Lxy_MinA0        .push_back(bplus_helper.lxy    (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_Lxy_MinA0_err    .push_back(bplus_helper.lxyErr (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_A0_MinA0         .push_back(bplus_helper.a0     (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_A0_MinA0_err     .push_back(bplus_helper.a0Err  (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_A0xy_MinA0       .push_back(bplus_helper.a0xy   (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_A0xy_MinA0_err   .push_back(bplus_helper.a0xyErr(xAOD::BPhysHelper::PV_MIN_A0));
        m_B_Z0_MinA0         .push_back(bplus_helper.z0     (xAOD::BPhysHelper::PV_MIN_A0));
        m_B_Z0_MinA0_err     .push_back(bplus_helper.z0Err  (xAOD::BPhysHelper::PV_MIN_A0));

        m_B_Lxy_MinZ0        .push_back(bplus_helper.lxy    (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_Lxy_MinZ0_err    .push_back(bplus_helper.lxyErr (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_A0_MinZ0         .push_back(bplus_helper.a0     (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_A0_MinZ0_err     .push_back(bplus_helper.a0Err  (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_A0xy_MinZ0       .push_back(bplus_helper.a0xy   (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_A0xy_MinZ0_err   .push_back(bplus_helper.a0xyErr(xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_Z0_MinZ0         .push_back(bplus_helper.z0     (xAOD::BPhysHelper::PV_MIN_Z0));
        m_B_Z0_MinZ0_err     .push_back(bplus_helper.z0Err  (xAOD::BPhysHelper::PV_MIN_Z0));

        m_h_bplus_pT ->Fill(bplus_ref_track.Pt      ());
        m_h_bplus_rap->Fill(bplus_ref_track.Rapidity());

        float bplus_mass = bplus_helper.mass();
        m_B_mass.push_back(bplus_mass);
        m_h_bplus_mass->Fill(bplus_mass);
        m_B_mass_err.push_back(bplus_helper.massErr());

        m_B_tau_MaxSumPt    .push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_tau_MaxSumPt_err.push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_B_tau_MinA0       .push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MIN_A0     ));
        m_B_tau_MinA0_err   .push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MIN_A0     ));
        m_B_tau_MinZ0       .push_back(bplus_helper.tau   (xAOD::BPhysHelper::PV_MIN_Z0     ));
        m_B_tau_MinZ0_err   .push_back(bplus_helper.tauErr(xAOD::BPhysHelper::PV_MIN_Z0     ));

        m_B_mu1_charge.push_back(bplus_helper.refTrkCharge(0));
        m_B_mu2_charge.push_back(bplus_helper.refTrkCharge(1));
        m_B_trk1_charge.push_back(bplus_helper.refTrkCharge(2));

        const xAOD::Vertex *orig_PV_MaxSumPt = bplus_helper.origPv(xAOD::BPhysHelper::PV_MAX_SUM_PT2);
        const xAOD::Vertex *orig_PV_MinA0    = bplus_helper.origPv(xAOD::BPhysHelper::PV_MIN_A0     );
        const xAOD::Vertex *orig_PV_MinZ0    = bplus_helper.origPv(xAOD::BPhysHelper::PV_MIN_Z0     );
        const xAOD::Vertex * ref_PV_MaxSumPt = bplus_helper.pv    (xAOD::BPhysHelper::PV_MAX_SUM_PT2);
        const xAOD::Vertex * ref_PV_MinA0    = bplus_helper.pv    (xAOD::BPhysHelper::PV_MIN_A0     );
        const xAOD::Vertex * ref_PV_MinZ0    = bplus_helper.pv    (xAOD::BPhysHelper::PV_MIN_Z0     );

        if(!ref_PV_MaxSumPt ) Error("execute()", "ref_PV_MaxSumPt Not Found");
        if(!ref_PV_MinZ0 ) Error("execute()", "ref_PV_MinZ0 Not Found");

        map_allIndices_to_selected[bplus_passes_counter++] = bplus_cand_counter;
        found_tagging_bcandidates.push_back(bplus_cand);

        std::unordered_map<const xAOD::Vertex *, double> sumPt; 
        std::unordered_map<const xAOD::Vertex *, double> sumPtSq;

        auto addifnot = [](std::unordered_map<const xAOD::Vertex*, double> &map, std::unordered_map<const xAOD::Vertex*, double> &map2, const xAOD::Vertex * vert)
        { if( map.count(vert) ==0){
                                      map[vert] = BPhysFunctions::SumPt(vert);
                                      map2[vert] = BPhysFunctions::SumPtSq(vert);
                                  }
        };

        addifnot(sumPt, sumPtSq, orig_PV_MaxSumPt);
        addifnot(sumPt, sumPtSq, orig_PV_MinA0);
        addifnot(sumPt, sumPtSq, orig_PV_MinZ0);
        addifnot(sumPt, sumPtSq, ref_PV_MaxSumPt);
        addifnot(sumPt, sumPtSq, ref_PV_MinA0);
        addifnot(sumPt, sumPtSq, ref_PV_MinZ0);

        m_MaxSumPt_SumPt.push_back(sumPt[ref_PV_MaxSumPt]);
        m_MinA0_SumPt.push_back(sumPt[ref_PV_MinA0]);
        m_MinZ0_SumPt.push_back(sumPt[ref_PV_MinZ0]);

        m_OrigMaxSumPt_SumPt.push_back(sumPt[ref_PV_MaxSumPt]);
        m_OrigMinA0_SumPt.push_back(sumPt[ref_PV_MinA0]);
        m_OrigMinZ0_SumPt.push_back(sumPt[ref_PV_MinZ0]);

        m_MaxSumPt_SumPtSq.push_back(sumPtSq[ref_PV_MaxSumPt]);
        m_MinA0_SumPtSq.push_back(sumPtSq[ref_PV_MinA0]);
        m_MinZ0_SumPtSq.push_back(sumPtSq[ref_PV_MinZ0]);

        m_OrigMaxSumPt_SumPtSq.push_back(sumPtSq[ref_PV_MaxSumPt]);
        m_OrigMinA0_SumPtSq.push_back(sumPtSq[ref_PV_MinA0]);
        m_OrigMinZ0_SumPtSq.push_back(sumPtSq[ref_PV_MinZ0]);


        
        float D0[3];
        float Z0[3];
        for(size_t i=0;i<3;i++){
            BPhysFunctions::track_to_vertex(trackptr[i], ref_PV_MinA0, D0+i, Z0+i );
        }
        m_B_mu1_extrapd0.push_back(D0[0]);
        m_B_mu2_extrapd0.push_back(D0[1]);
        m_B_trk1_extrapd0.push_back(D0[2]);

        m_B_mu1_d0.push_back(trackptr[0]->d0());
        m_B_mu2_d0.push_back(trackptr[1]->d0());
        m_B_trk1_d0.push_back(trackptr[2]->d0());


        m_mu1_coVar.push_back(trackptr[0]->definingParametersCovMatrixVec ());
        m_mu2_coVar.push_back(trackptr[1]->definingParametersCovMatrixVec ());
        m_had1_coVar.push_back(trackptr[2]->definingParametersCovMatrixVec ());


        m_PV_minA0_x.push_back(ref_PV_MinA0->x());
        m_PV_minA0_y.push_back(ref_PV_MinA0->y());
        m_PV_minA0_z.push_back(ref_PV_MinA0->z());

        m_orig_PV_minA0_x.push_back(orig_PV_MinA0->x());
        m_orig_PV_minA0_y.push_back(orig_PV_MinA0->y());
        m_orig_PV_minA0_z.push_back(orig_PV_MinA0->z());


        m_SecVtx_x.push_back(bplus_cand->x());
        m_SecVtx_y.push_back(bplus_cand->y());
        m_SecVtx_z.push_back(bplus_cand->z());


        m_MaxSumPt_orig_ntrk .push_back(orig_PV_MaxSumPt->nTrackParticles());
        m_MinA0_orig_ntrk    .push_back(orig_PV_MinA0   ->nTrackParticles());
        m_MinZ0_orig_ntrk    .push_back(orig_PV_MinZ0   ->nTrackParticles());
        m_MaxSumPt_ref_ntrk  .push_back( ref_PV_MaxSumPt->nTrackParticles());
        m_MinA0_ref_ntrk     .push_back( ref_PV_MinA0   ->nTrackParticles());
        m_MinZ0_ref_ntrk     .push_back( ref_PV_MinZ0   ->nTrackParticles());
        m_MaxSumPt_ref_stat  .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MAX_SUM_PT2));
        m_MinA0_ref_stat     .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MIN_A0     ));
        m_MinZ0_ref_stat     .push_back(bplus_helper.RefitPVStatus(xAOD::BPhysHelper::PV_MIN_Z0     ));
        m_MaxSumPt_vertextype.push_back( ref_PV_MaxSumPt->vertexType());
        m_MinA0_vertextype   .push_back( ref_PV_MinA0   ->vertexType());
        m_MinZ0_vertextype   .push_back( ref_PV_MinZ0   ->vertexType());
      
}


if (m_B_chi2_ndof.empty())
{
    ClearBranches();
    return EL::StatusCode::SUCCESS;
}


if(m_trig_decision_tool){
    const size_t trigsize = triggers.size();
    if (m_trigger_branches.size() != 2*trigsize)
    {
        Error("execute()", "Internal logic error: m_trigger_branches.size() != triggers.size()");
        return EL::StatusCode::FAILURE;
    }
    for (size_t i = 0; i < triggers.size(); i++)
    {
        auto chain_group = m_trig_decision_tool->getChainGroup(triggers[i].c_str());
        *(m_trigger_branches[i+trigsize]) = chain_group->getPrescale();
        *(m_trigger_branches[i])          = chain_group->isPassed();
    }



    auto chainGroup = m_trig_decision_tool->getChainGroup("HLT_2mu4_bBmumuxv2");
    auto fc = chainGroup->features();
    auto bphContainers = fc.containerFeature<xAOD::TrigBphysContainer>("EFBMuMuXFex");
    m_HLT_2mu4_bBmumuxv2_EFBMuMuXFex_type9 = false;
    for(auto bphFeature : bphContainers) {
        auto bphContainer = bphFeature.cptr();
        for(auto bph : *bphContainer) {
            m_HLT_2mu4_bBmumuxv2_EFBMuMuXFex_type9 = m_HLT_2mu4_bBmumuxv2_EFBMuMuXFex_type9 | (bph->particleType() == 9);
        }
    }
}
if(m_L1Trig && m_trig_decision_tool){

    std::vector<std::string> L1items = m_trig_decision_tool->getChainGroup("L1_.*")->getListOfTriggers() ;
    m_L1Trig->resize(L1items.size());
    m_passedbits->resize(L1items.size());
    m_passedbeforeprescale->resize(L1items.size());
    size_t i=0;
    for(const auto &str : L1items){
        auto cg = m_trig_decision_tool->getChainGroup(str);
        (*m_L1Trig)    [i] = str;
        (*m_passedbits)[i] = cg->isPassedBits();
        (*m_passedbeforeprescale)[i] = cg->isPassedBits() & TrigDefs::L1_isPassedBeforePrescale;
        i++;
    }
    m_L1Tree->Fill();
}


static bool parse_triggers = true;
if (parse_triggers && m_trig_decision_tool)
{
    parse_triggers = false;
    Info("execute()", "Parsing triggers");
    auto chain_group = m_trig_decision_tool->getChainGroup(print_trigger_regexp.c_str());
    for(auto &trig : chain_group->getListOfTriggers())
    {
        std::string this_trig = trig;
        Info("execute()", "%60s", this_trig.c_str());
    }
}


size_t lowest_chi2_index = 0;
for (size_t i = 1; i < m_B_chi2_ndof.size(); i++)
{
    if (m_B_chi2_ndof[i] < m_B_chi2_ndof[lowest_chi2_index])
        lowest_chi2_index = i;
}


IBranchVariable::SetAllElementsToStore(lowest_chi2_index, this);
if(is_mc) IBranchVariable::SetAllElementsToStore(lowest_chi2_index, this, 2);
IBranchVariable::SetAllElementsToStore(lowest_chi2_index, this, 1); // 1 - trigger branches group


if(!m_DisableAllCands) m_tree        ->Fill();
m_tree_single ->Fill();
if(m_tree_trigger) m_tree_trigger->Fill();
if(m_treeMuon) m_treeMuon->Fill();
ClearBranches();
return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusNtupleMaker::postExecute()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode BplusNtupleMaker::finalize()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.

    //xAOD::TEvent* event = wk()->xaodEvent();

    Info("finalize()", "Number of B candidates (loose): %i", m_n_B_candidates_loose_chi);
    Info("finalize()", "Number of B candidates (tight): %i", m_n_B_candidates_tight_chi);
    Info("finalize()", "Number of Jpsi candidates: %i"     , m_n_Jpsi_candidates       );
    Info("finalize()", "Number of events with 0 jpsis: %i" , m_n_events_with_zero_jpsis);
    Info("finalize()", "Cut out by GRL: %i"                , m_skip_grl                );
    Info("finalize()", "Cut out by selection cuts: %i"     , m_skip_cuts               );


    Info("finalize()", "TrueDecaysFound: %i"     , TrueDecaysFound           );
    Info("finalize()", "TrueDecaysFoundOver1: %i"     , TrueDecaysFoundOver1           );
    Info("finalize()", "FullReconFound: %i"     , FullReconFound           );

    delete m_trig_decision_tool;
    m_trig_decision_tool = nullptr;
    delete m_trig_config_tool;
    m_trig_config_tool = nullptr;
    delete m_grl;
    m_grl = nullptr;

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode BplusNtupleMaker::histFinalize()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    for (auto &trig_branch : m_trigger_branches)
    {
        delete trig_branch;
        trig_branch = nullptr;
    }
    m_trigger_branches.clear();
    if(m_L1Trig){
        delete m_L1Trig; m_L1Trig = nullptr;
        delete m_passedbits; m_passedbits = nullptr;
        delete m_passedbeforeprescale; m_passedbeforeprescale= nullptr;
    }
    return EL::StatusCode::SUCCESS;
}

void BplusNtupleMaker::InitBranches()
{

    //Process trigger list to eliminate duplicates
    std::vector<std::string> newlist;
    std::unordered_set<size_t> set;

    for(const auto &str : triggers){
       auto hash = std::hash<std::string>{}(str);
       if(set.count(hash) ==0){
          newlist.push_back(str);
          set.insert(hash);
       }
    }

    triggers = newlist;

    triggers.shrink_to_fit();

    for (std::string &trigger : newlist) //replace problematic '-' characters
    {
        std::replace(trigger.begin(), trigger.end  (), '-', '_');
    }

    if(!DisableTrigger){
     const std::vector<std::string> &copytriggers = newlist;

     m_trigger_branches.reserve(copytriggers.size() *2);
     for (const std::string &trigger : copytriggers)
     {
         m_trigger_branches.push_back(new ScalarBranchVariable<int>(trigger.c_str(), this, 1)); // 1 - trigger branches group
     }
     const std::string ps("_prescale");
     std::string prescalestring;
     for (const std::string &trigger : copytriggers) //Book prescales
     {
         prescalestring = trigger;
         prescalestring += ps;
         m_trigger_branches.push_back(new ScalarBranchVariable<int>(prescalestring.c_str(), this, 1)); // 1 - trigger branches group
     }
    }


    IBranchVariable::SetAllBranches(m_tree,         m_tree_single, this);

    if(!DisableTrigger) IBranchVariable::SetAllBranches(m_tree_trigger, nullptr      , this, 1); // 1 - trigger branches group
    if(m_treeMuon) IBranchVariable::SetAllBranches(m_treeMuon, nullptr, this, 5);
    //MC branches (2) initialised in different place
}

void BplusNtupleMaker::ClearBranches()
{
    IBranchVariable::ResetAllBranches(this);
    IBranchVariable::ResetAllBranches(this, 1); // 1 - trigger branches group
    IBranchVariable::ResetAllBranches(this, 2);
    IBranchVariable::ResetAllBranches(this, 5);
}

