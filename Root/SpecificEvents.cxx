#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "xAODTrigBphys/TrigBphysContainer.h"
#include <BplusxAODAnalysis/SpecificEvents.h>
#include "BPhysxAODTools/BPhysFunctions.h"
#include "TSystem.h"
#include <TH1.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(SpecificEvents)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

SpecificEvents :: SpecificEvents ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode SpecificEvents :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree = new TTree("Triggers"      , "Triggers"      );
  m_tree ->SetDirectory(output_file);
  IBranchVariable::SetAllBranches(m_tree,         nullptr, this);
  m_tree->Branch("TriggerList", &m_Triggers);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.


  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

  m_event_counter = 0;



  {
    m_trig_config_tool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    EL_RETURN_CHECK("initialize()", m_trig_config_tool->initialize());
    ToolHandle<TrigConf::ITrigConfigTool> trig_config_handle(m_trig_config_tool);
    m_trig_decision_tool = new Trig::TrigDecisionTool("TrigDecisionTool");
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("ConfigTool", trig_config_handle)); // connect the TrigDecisionTool to the ConfigTool
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("TrigDecisionKey", "xTrigDecision"));
    EL_RETURN_CHECK("initialize()", m_trig_decision_tool->initialize());
  }

  uint32_t run;
  unsigned long long eventN;

  TFile *file = new TFile(EventFile, "READ");
  TTree *eventtree = (TTree*) file->Get("events");
  eventtree->SetBranchAddress("run", &run);
  eventtree->SetBranchAddress("event", &eventN);

  int N = eventtree->GetEntries();
  for(int i =0;i<N;i++) {
     eventtree->GetEntry(i);
     m_Events[run].push_back(eventN);
  }
  delete file;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 100) == 0)
    Info("execute()", "%i events prcessed so far", m_event_counter);

  m_event_counter++;

  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));

  bool newrun = m_run_number != event_info->runNumber  ();


  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();
 
  if(newrun){
     lastrun = m_Events.find(m_run_number);
  }
  if(lastrun == m_Events.end()) return EL::StatusCode::SUCCESS;

  if(std::find(lastrun->second.begin(), lastrun->second.end(), m_evt_number) == lastrun->second.end()) return EL::StatusCode::SUCCESS;

  auto chain_group = m_trig_decision_tool->getChainGroup("HLT_.*");

    if(chain_group !=nullptr){
      auto list = chain_group->getListOfTriggers();
      for(auto &trigstring : list)
        {
//            auto chain1 = m_trigDecisionTool->getChainGroup(trigstring.c_str());
            bool pass = m_trig_decision_tool->isPassed(trigstring);
            if(pass) m_Triggers.push_back(trigstring);
        }
    }else
        Error("execute()", "Trigger returned null 429");

  

  m_tree->Fill();
  IBranchVariable::ResetAllBranches(this);
  m_Triggers.clear();
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SpecificEvents :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
