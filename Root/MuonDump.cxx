#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <BplusxAODAnalysis/MuonDump.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include <TTree.h>
#include "TFile.h"


// this is needed to distribute the algorithm to the workers
ClassImp(MuonDump)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

//typedef ElementLink<xAOD::VertexContainer> VertexLink;
//typedef std::vector<VertexLink> VertexLinkVector;

MuonDump :: MuonDump () : output_name         ("DefaultOutput")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode MuonDump :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree         = new TTree("Muon", "Muon");
  m_tree       ->SetDirectory(output_file);
  IBranchVariable::SetAllBranches(m_tree,         0, this);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: changeInput (bool)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();
  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));

  m_run_number = event_info->runNumber();
  m_lumi_block = event_info->lumiBlock();
  m_evt_number = event_info->eventNumber();


  averageInteractionsPerCrossing=  event_info->averageInteractionsPerCrossing();
  actualInteractionsPerCrossing =  event_info->actualInteractionsPerCrossing();
  const xAOD::TrackParticleContainer* tracks = 0;
  if(!event->retrieve( tracks, "InDetTrackParticles" ).isSuccess()) {
     Error("execute()", "Failed to find InDetTrackParticles");
     return EL::StatusCode::FAILURE;
  }
  
  for(auto trk : *tracks){
     m_mu_pT = trk->pt();
     m_mu_eta = trk->eta();
     m_mu_phi = trk->phi();
     m_mu_charge = trk->charge();
     m_mu_phi0   = trk->phi0();
     m_mu_d0 = trk->d0();
     m_mu_z0 = trk->z0();
     m_mu_theta = trk->theta();
     m_mu_qOverP = trk->qOverP();
     
     
     m_tree->Fill();
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MuonDump :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


