#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <BplusxAODAnalysis/ParticleGun.h>
#include "BPhysxAODTools/BPhysFunctions.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "TFile.h"
#include <iostream>

// this is needed to distribute the algorithm to the workers
ClassImp(ParticleGun)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )



ParticleGun :: ParticleGun () :
     output_name         ("DefaultOutput"), m_event_counter(0)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode ParticleGun :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  m_tree         = new TTree("RecoTracks", "RecoTracks");
  m_tree ->SetDirectory(wk()->getOutputFile(output_name));
  m_tree->Branch("True_pT", &m_True_pT);
  m_tree->Branch("True_eta", &m_True_eta);
  m_tree->Branch("True_phi", &m_True_phi);
//  m_tree->Branch("True_d0", &m_True_d0);
//  m_tree->Branch("True_z0", &m_True_z0);
  
  m_tree->Branch("PDG", &m_PDG);
  m_tree->Branch("qoverP", &m_qoverP);

  m_tree->Branch("Reco_pT", &m_Reco_pT);
  m_tree->Branch("Reco_pTErr", &m_Reco_pTErr);
  m_tree->Branch("Reco_eta", &m_Reco_eta);
  m_tree->Branch("Reco_phi", &m_Reco_phi);
  m_tree->Branch("Reco_d0", &m_Reco_d0);
  m_tree->Branch("Reco_d0Err", &m_Reco_d0Err);
  m_tree->Branch("Reco_z0", &m_Reco_z0);

  m_tree->Branch("run", &m_run_number);
  m_tree->Branch("event", &m_evt_number);
  m_tree->Branch("lumi", &m_lumi_block);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: changeInput (bool )
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 100) == 0)
    Info("execute()", "%i events prcessed so far", (int) m_event_counter);
  ++m_event_counter;
  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));


  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();

  std::vector<float>* list[] = {  &m_True_pT,  &m_True_eta, &m_True_phi,  &m_qoverP, &m_Reco_pT, &m_Reco_pTErr, &m_Reco_eta,
                                  &m_Reco_phi, &m_Reco_d0, &m_Reco_d0Err, &m_Reco_z0};

  constexpr int N = sizeof(list)/sizeof(list[0]);
  for(int i =0;i<N;i++) list[i]->clear();
  m_PDG.clear();

  const xAOD::TrackParticleContainer* tracks = nullptr;
  if(!event->retrieve( tracks, "InDetTrackParticles" ).isSuccess()) {
     Error("execute()", "Failed to find InDetTrackParticles");
     return EL::StatusCode::FAILURE;
  }

  for(auto track : *tracks){
     auto truth = BPhysFunctions::getTrkGenParticle(track);
     
     m_True_pT.push_back(truth ? truth->pt() : 0);
     m_True_eta.push_back(truth ? truth->eta() : -99);
     m_True_phi.push_back(truth ? truth->phi() : -99);
     m_PDG.push_back(truth ? truth->pdgId() : 0);
     m_qoverP.push_back(track->qOverP() );
     m_Reco_pT.push_back(track->pt());
    
     m_Reco_eta.push_back(track->eta());
     m_Reco_phi.push_back(track->phi());
     m_Reco_d0.push_back(track->d0());

     m_Reco_z0.push_back(track->z0());

     auto matrix = track->definingParametersCovMatrix();
//     auto invm = matrix.inverse().eval();
//     std::cout << matrix << std::endl;
//     Info("execute()", "matrix 0 0 = %f d0 = %f", std::sqrt(matrix(0,0)), track->d0());

     float phi = track->phi();
     float theta = track->theta();

     auto p4 = track->p4();
     float Px = p4.X();
     float Py = p4.Y();
     float qOverP = track->qOverP();
     float charge = track->charge();
     double dpxdqOverP = -(sin(theta)*cos(phi)*charge)/(qOverP*qOverP);
     double dpxdtheta  =  (cos(theta)*cos(phi)*charge)/qOverP;
     double dpxdphi    = -(sin(theta)*sin(phi)*charge)/qOverP;
     double dpydqOverP = -(sin(theta)*sin(phi)*charge)/(qOverP*qOverP);
     double dpydtheta  =  (cos(theta)*sin(phi)*charge)/qOverP;
     double dpydphi    =  (sin(theta)*cos(phi)*charge)/qOverP;

     float PT = track->pt();
     double dPTdqOverP  = (Px*dpxdqOverP+Py*dpydqOverP)/PT;
     double dPTdtheta   = (Px*dpxdtheta+Py*dpydtheta)/PT;
     double dPTdphi     = (Px*dpxdphi+Py*dpydphi)/PT;
     Amg::MatrixX PtErrSq(1,1);

     Amg::MatrixX D_vec(5*1,1); D_vec.setZero();
     D_vec(2,0)  = dPTdphi;
     D_vec(3,0)  = dPTdtheta;
     D_vec(4,0)  = dPTdqOverP;
     PtErrSq = D_vec.transpose() * matrix * D_vec;
     double pterr =  PtErrSq(0,0) >0. ? std::sqrt(PtErrSq(0,0)) : 0. ;
//     Info("execute()", "pt = %f +/- %f", PT, pterr);
     m_Reco_d0Err.push_back(std::sqrt(matrix(0,0)));
     m_Reco_pTErr.push_back(pterr);
  }

  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode ParticleGun :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
