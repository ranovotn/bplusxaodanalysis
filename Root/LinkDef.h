#include <BplusxAODAnalysis/ParticleGun.h>

#include <BplusxAODAnalysis/SpecificEvents.h>

#include <BplusxAODAnalysis/MuonDump.h>

#include <BplusxAODAnalysis/TriggerTest.h>

#include <BplusxAODAnalysis/BplusNtupleMaker.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class BplusNtupleMaker+;
#endif

#ifdef __CINT__
#pragma link C++ class TriggerTest+;
#endif

#ifdef __CINT__
#pragma link C++ class MuonDump+;
#endif

#ifdef __CINT__
#pragma link C++ class SpecificEvents+;
#endif

#ifdef __CINT__
#pragma link C++ class ParticleGun+;
#endif
