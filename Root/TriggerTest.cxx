#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODTracking/VertexContainer.h"
#include <BplusxAODAnalysis/TriggerTest.h>
#include <TrigConfxAOD/xAODConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include "xAODTrigBphys/TrigBphysContainer.h"
#include "xAODTrigBphys/TrigBphys.h"
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>


// this is needed to distribute the algorithm to the workers
ClassImp(TriggerTest)

#define EL_RETURN_CHECK( CONTEXT, EXP )                    \
  do {                                                     \
     if( ! EXP.isSuccess() ) {                             \
        Error( CONTEXT,                                    \
               XAOD_MESSAGE( "Failed to execute: %s" ),    \
               #EXP );                                     \
        return EL::StatusCode::FAILURE;                    \
     }                                                     \
   } while( false )

typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;


TriggerTest :: TriggerTest ()  :output_name         ("DefaultOutput"), jpsi_container_name ("BPHY1OniaCandidates"),
    
    print_trigger_regexp("")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode TriggerTest :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
  TFile *output_file = wk()->getOutputFile(output_name);
  m_tree_trigger = new TTree("Triggers", "Triggers");
  m_tree_trigger->SetDirectory(output_file);
  
  m_tree         = new TTree("Event", "Event");
  m_tree->SetDirectory(output_file);
  InitBranches();

  return EL::StatusCode::SUCCESS;
}

void TriggerTest::InitBranches()
  {
  
  std::vector<std::string> copytriggers(triggers); //copy for work in method
  for (std::string &trigger : copytriggers) //replace problematic '-' characters
  {
      std::replace(trigger.begin(),
                 trigger.end  (),
                 '-', '_');
  }
 
  for (const std::string &trigger : copytriggers)
  {
    m_trigger_branches.push_back(new ScalarBranchVariable<int>(trigger.c_str(), this, 1)); // 1 - trigger branches group
  }
  const std::string ps("_prescale");
  for (std::string &trigger : copytriggers) //Add prescale
  {
    trigger += ps;
  }

  for (const std::string &trigger : copytriggers) //Book prescales
  {
    m_trigger_branches.push_back(new ScalarBranchVariable<int>(trigger.c_str(), this, 1)); // 1 - trigger branches group
  }
  
  IBranchVariable::SetAllBranches(m_tree, nullptr, this);
  IBranchVariable::SetAllBranches(m_tree_trigger, nullptr      , this, 1); // 1 - trigger branches group
  }

void TriggerTest::ClearBranches()
  {
  IBranchVariable::ResetAllBranches(this);
  IBranchVariable::ResetAllBranches(this, 1); // 1 - trigger branches group
  }

EL::StatusCode TriggerTest :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: changeInput (bool)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int  
  m_event_counter= m_n_events_with_zero_jpsis= 0;
  
  m_trig_config_tool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
  EL_RETURN_CHECK("initialize()", m_trig_config_tool->initialize());
  ToolHandle<TrigConf::ITrigConfigTool> trig_config_handle(m_trig_config_tool);
  m_trig_decision_tool = new Trig::TrigDecisionTool("TrigDecisionTool");
  EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("ConfigTool", trig_config_handle)); // connect the TrigDecisionTool to the ConfigTool
  EL_RETURN_CHECK("initialize()", m_trig_decision_tool->setProperty("TrigDecisionKey", "xTrigDecision"));
  EL_RETURN_CHECK("initialize()", m_trig_decision_tool->initialize());  
  
  
  return EL::StatusCode::SUCCESS;
}

//HLT_mu6_mu4_bJpsimumu && HLT_mu6_mu4_bBmumuxv2 && xAOD::TrigBphys::BSPHIMUMU

EL::StatusCode TriggerTest :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  xAOD::TEvent *event = wk()->xaodEvent();


  if ((m_event_counter % 1000) == 0)
    Info("execute()", "%i events prcessed so far", m_event_counter);

  m_event_counter++;
  
  const xAOD::EventInfo *event_info = nullptr;
  EL_RETURN_CHECK("execute()", event->retrieve(event_info, "EventInfo"));
  m_evt_number = event_info->eventNumber();
  m_lumi_block = event_info->lumiBlock  ();
  m_run_number = event_info->runNumber  ();
  
//  const xAOD::VertexContainer *container_jpsi = nullptr;
//  EL_RETURN_CHECK("execute()", event->retrieve(container_jpsi, jpsi_container_name.c_str()));  

//  m_n_events_with_zero_jpsis+=container_jpsi->empty();  
  
  const size_t trigsize = triggers.size();
  
 if (m_trigger_branches.size() != 2*trigsize)
    {
    Error("execute()", "Internal logic error: m_trigger_branches.size() != 2*triggers.size()");
    return EL::StatusCode::FAILURE;
    }
  for (size_t i = 0; i < trigsize; i++)
   {
    auto chain_group = m_trig_decision_tool->getChainGroup(triggers[i].c_str());
    if(chain_group == nullptr){ //error handling
       *(m_trigger_branches[i]) = 0;
       *(m_trigger_branches[i+trigsize]) = 0;
       continue;
    }
    *(m_trigger_branches[i+trigsize]) = chain_group->getPrescale();    
    
    if(chain_group->isPassed()){
      auto fc = chain_group->features();
      auto bphContainers = fc.containerFeature<xAOD::TrigBphysContainer>("EFBMuMuXFex");

      *(m_trigger_branches[i]) = 1;
      for(auto &bphFeature : bphContainers) {
        for(auto bph : *bphFeature.cptr()) {
          if (bph->particleType() == xAOD::TrigBphys::BSPHIMUMU) { *(m_trigger_branches[i]) = 2; goto exitloop;}
        }
      }
     }else{
       *(m_trigger_branches[i]) = 0;
     }
exitloop:
     continue;
   }


  static bool parse_triggers = true;
  if (parse_triggers)
    {
    parse_triggers = false;
    Info("execute()", "Parsing triggers");
    auto chain_group = m_trig_decision_tool->getChainGroup(print_trigger_regexp.c_str());
    for(auto &trig : chain_group->getListOfTriggers())
      {
      std::string this_trig = trig;
      Info("execute()", "%60s", this_trig.c_str());
      } 
    }  
  
  
/*  
    for (auto jpsi_cand : (*container_jpsi))
    {
    const xAOD::Vertex *jpsi_vertex = (const xAOD::Vertex *)jpsi_cand;
    xAOD::BPhysHelper jpsi_helper(jpsi_cand);
    if(jpsi_helper.nRefTrks() != 2 ||
       jpsi_helper.nMuons()   != 2)
      {
      Warning("execute()", "Expected two muon tracks, skip");
      continue;
      }
    TLorentzVector muon0_ref_track = jpsi_helper.refTrk(0, 105.65837);
    TLorentzVector muon1_ref_track = jpsi_helper.refTrk(1, 105.65837);
    TLorentzVector jpsi_ref_track = muon0_ref_track + muon1_ref_track;
    

    float jpsi_mass = jpsi_vertex->auxdata<float>("Jpsi_mass");
    m_Jpsi_mass    .push_back(jpsi_mass);
    m_Jpsi_rapidity.push_back(jpsi_ref_track.Rapidity());
    m_Jpsi_pT      .push_back(jpsi_ref_track.Pt      ());
    m_Jpsi_chi2    .push_back(jpsi_helper.vtx()->chiSquared());
    m_Jpsi_mu1_eta .push_back(muon0_ref_track.Eta());
    m_Jpsi_mu1_pT  .push_back(muon0_ref_track.Pt ());
    m_Jpsi_mu2_eta .push_back(muon1_ref_track.Eta());
    m_Jpsi_mu2_pT  .push_back(muon1_ref_track.Pt ());
    m_B_Jpsi_index.push_back(m_Jpsi_mass.size());
    }
*/  
  m_tree        ->Fill();
  m_tree_trigger->Fill();
  ClearBranches();  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  delete m_trig_decision_tool;
  m_trig_decision_tool = nullptr;
  delete m_trig_config_tool;
  m_trig_config_tool = nullptr;
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerTest :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  for (auto &trig_branch : m_trigger_branches)
    {
    delete trig_branch;
    trig_branch = nullptr;
    }
  m_trigger_branches.clear();  
  
  return EL::StatusCode::SUCCESS;
}
