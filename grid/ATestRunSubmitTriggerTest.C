void ATestRunSubmitTriggerTest()
  {
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE 
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  
  bool submittoGrid = true;
  std::string submitDir = "outputnewntuple";
  xAOD::Init().ignore();
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // inputFilePath needsto be changed to your local input file 


  if(submittoGrid){
       SH::scanRucio(sh,"user.abarton.AOD.f705_m1606.run00300345to00302925.ntple.1");
       sh.setMetaString("nc_grid_filter", "*");
  }else{
       const char *inputFilePath = gSystem->ExpandPathName("/scratch/ab/data16test/data16_13TeV.00300345.physics_Main.merge.AOD.f705_m1606");
       SH::ScanDir().sampleDepth(0).samplePattern("*.root").scan(sh, inputFilePath);
  }
  sh.setMetaString("nc_tree", "CollectionTree");

  sh.print();

  EL::Job job;
  job.sampleHandler(sh);
  // job.options()->setDouble(EL::Job::optSkipEvents, 1);
  // job.options()->setDouble(EL::Job::optMaxEvents, 10);

  const char *output_name = "DefaultOutput";
  EL::OutputStream output(output_name);
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_name);
  job.algsAdd(ntuple);

  TriggerTest *alg = new TriggerTest;
  
alg->triggers.push_back("L1_MU6_2MU4");  ////32031
alg->triggers.push_back("L1_2MU6");  ////24942
alg->triggers.push_back("L1_MU10_2MU6");  ////22311
alg->triggers.push_back("L1_MU11_2MU6");  ////20168
alg->triggers.push_back("L1_MU20");  ////17478
alg->triggers.push_back("L1_2MU10");  ////13761
alg->triggers.push_back("L1_EM20VHI");  ////8806
alg->triggers.push_back("L1_EM8I_MU10");  ////8735
alg->triggers.push_back("HLT_2mu6_bJpsimumu_delayed");  ////8666
alg->triggers.push_back("L1_EM22VHI");  ////8553
alg->triggers.push_back("L1_EM24VHI");  ////8285
alg->triggers.push_back("L1_TAU60");  ////7978
alg->triggers.push_back("L1_MU10_2J15_J20");  ////7720
alg->triggers.push_back("L1_EM15VH_3EM7");  ////7360
alg->triggers.push_back("L1_MU6_3MU4");  ////7262
alg->triggers.push_back("L1_MU6_J30.0ETA49_2J20.0ETA49");  ////7162
alg->triggers.push_back("L1_J100");  ////6988
alg->triggers.push_back("HLT_j0_perf_ds1_L1J100");  ////6983
alg->triggers.push_back("L1_EM15I_MU4");  ////6977
alg->triggers.push_back("L1_EM15HI_TAU40_2TAU15");  ////6952
alg->triggers.push_back("HLT_2mu6_bJpsimumu_Lxy0_delayed");  ////6810
alg->triggers.push_back("L1_TAU20IM_2TAU12IM_J25_2J20_3J12");  ////6740
alg->triggers.push_back("HLT_mu10_mu6_bJpsimumu_delayed");  ////6668
alg->triggers.push_back("L1_EM13VH_3J20");  ////6573
alg->triggers.push_back("HLT_2mu6_bBmumuxv2_delayed");  ////6356
alg->triggers.push_back("L1_2MU6_3MU4");  ////6351
alg->triggers.push_back("L1_MU10_2J20");  ////6278
alg->triggers.push_back("L1_MU10_TAU12IM_J25_2J12");  ////6185
alg->triggers.push_back("L1_MU10_TAU20IM");  ////6179
alg->triggers.push_back("L1_J75_3J20");  ////6100
alg->triggers.push_back("L1_XE50");  ////6038
alg->triggers.push_back("L1_TAU40_2TAU20IM");  ////5984
alg->triggers.push_back("L1_3J25.0ETA23");  ////5979
alg->triggers.push_back("L1_4J15");  ////5853
alg->triggers.push_back("L1_J75_XE40");  ////5576
alg->triggers.push_back("L1_J120");  ////5522
alg->triggers.push_back("L1_J40_XE50");  ////5370
alg->triggers.push_back("HLT_mu10_mu6_bJpsimumu_Lxy0_delayed");  ////5342
alg->triggers.push_back("L1_4J15.0ETA25");  ////5228
alg->triggers.push_back("L1_EM15HI_2TAU12IM_J25_3J12");  ////5164
alg->triggers.push_back("HLT_mu10_mu6_bBmumuxv2_delayed");  ////4907
alg->triggers.push_back("L1_2EM13VH");  ////4807
alg->triggers.push_back("L1_EM15VH_MU10");  ////4697
alg->triggers.push_back("L1_TAU20IM_2J20_XE45");  ////4697
alg->triggers.push_back("L1_XE55");  ////4641
alg->triggers.push_back("L1_TAU20IM_2TAU12IM_XE35");  ////4590
alg->triggers.push_back("L1_2EM15VH");  ////4350
alg->triggers.push_back("L1_4J20");  ////4341
alg->triggers.push_back("HLT_mu20_l2idonly_mu6noL1_nscan03");  ////4224
alg->triggers.push_back("HLT_mu20_nomucomb_mu6noL1_nscan03");  ////4172
alg->triggers.push_back("L1_2J15_XE55");  ////3993
alg->triggers.push_back("L1_MU6_J75");  ////3962
alg->triggers.push_back("L1_MU6_J40");  ////3808
alg->triggers.push_back("L1_EM15HI_2TAU12IM_XE35");  ////3797
alg->triggers.push_back("HLT_2mu6_bUpsimumu_delayed");  ////3753
alg->triggers.push_back("HLT_mu6_mu4_bBmumu");  ////3686
alg->triggers.push_back("L1_XE60");  ////3600
alg->triggers.push_back("L1_3MU6");  ////3502
alg->triggers.push_back("HLT_mu20_2mu0noL1_JpsimumuFS");  ////3432
alg->triggers.push_back("HLT_mu20_mu8noL1");  ////3370
alg->triggers.push_back("HLT_mu6_mu4_bBmumu_Lxy0");  ////3337
alg->triggers.push_back("HLT_2mu10_bJpsimumu_noL2");  ////3264
alg->triggers.push_back("HLT_2mu10_bJpsimumu_delayed");  ////3116
alg->triggers.push_back("HLT_2mu10_bJpsimumu");  ////3116
alg->triggers.push_back("L1_2EM8VH_MU10");  ////3002
alg->triggers.push_back("L1_2J50_XE40");  ////2999
alg->triggers.push_back("L1_MU10_TAU12IM_XE35");  ////2973
alg->triggers.push_back("HLT_mu24_ivarmedium");  ////2959
alg->triggers.push_back("HLT_mu24_imedium");  ////2876
alg->triggers.push_back("L1_3J50");  ////2844
alg->triggers.push_back("HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4");  ////2834
alg->triggers.push_back("L1_MU10_3J20");  ////2812
alg->triggers.push_back("HLT_mu22_mu8noL1");  ////2561
alg->triggers.push_back("HLT_mu11_nomucomb_mu6noL1_nscan03_L1MU11_2MU6_bTau_delayed");  ////2558
alg->triggers.push_back("HLT_mu20_2mu4_JpsimumuL2");  ////2555
alg->triggers.push_back("HLT_xe90_mht_L1XE50");  ////2531
alg->triggers.push_back("L1_MU15");  ////2439
alg->triggers.push_back("L1_2MU4");  ////2415
alg->triggers.push_back("L1_2MU20_OVERLAY");  ////2343
alg->triggers.push_back("HLT_mu10_mu6_bUpsimumu_delayed");  ////2330
alg->triggers.push_back("HLT_mu10_mu6_bUpsimumu");  ////2330
alg->triggers.push_back("HLT_mu26_ivarmedium");  ////2258
alg->triggers.push_back("L1_XE70");  ////2212
alg->triggers.push_back("L1_TAU40");  ////2201
alg->triggers.push_back("L1_5J15.0ETA25");  ////2197
alg->triggers.push_back("HLT_2mu10_bBmumuxv2");  ////2194
alg->triggers.push_back("HLT_2mu10_bBmumuxv2_delayed");  ////2194
alg->triggers.push_back("HLT_mu26_imedium");  ////2191
alg->triggers.push_back("L1_3J20");  ////2159
alg->triggers.push_back("HLT_2mu6_bJpsimumu");  ////2151
alg->triggers.push_back("HLT_xe90_mht_wEFMu_L1XE50");  ////2136
alg->triggers.push_back("L1_J75");  ////2115
alg->triggers.push_back("HLT_mu24_mu8noL1");  ////2030
alg->triggers.push_back("HLT_mu6_mu4_bJpsimumu_Lxy0_delayed");  ////1992
alg->triggers.push_back("HLT_2mu6_bUpsimumu");  ////1983
alg->triggers.push_back("HLT_3mu6_msonly");  ////1965
alg->triggers.push_back("HLT_mu6_mu4_bBmumuxv2_delayed");  ////1863
alg->triggers.push_back("HLT_mu6_mu4_bBmumuxv2");  ////1863

alg->triggers.push_back("HLT_mu6_2mu4_bJpsi_delayed");  ////1808
alg->triggers.push_back("HLT_xe90_mht_L1XE55");  ////1794
alg->triggers.push_back("L1_MU6_J20");  ////1745
alg->triggers.push_back("HLT_mu28_ivarmedium");  ////1731
alg->triggers.push_back("HLT_mu28_imedium");  ////1674
alg->triggers.push_back("HLT_2mu6_bBmumu");  ////1667
alg->triggers.push_back("HLT_mu11_nomucomb_mu6noL1_nscan03_L1MU11_2MU6_bTau");  ////1626
alg->triggers.push_back("HLT_2mu6_bJpsimumu_Lxy0");  ////1622
alg->triggers.push_back("HLT_mu26_mu8noL1");  ////1597
alg->triggers.push_back("HLT_2mu14");  ////1577
alg->triggers.push_back("HLT_2mu14_nomucomb");  ////1540
alg->triggers.push_back("HLT_xe100_mht_L1XE50");  ////1534
alg->triggers.push_back("HLT_mu6_mu4_bJpsimumu");  ////1529
alg->triggers.push_back("HLT_mu6_mu4_bUpsimumu_delayed");  ////1483
alg->triggers.push_back("HLT_xe90_mht_wEFMu_L1XE55");  ////1475
alg->triggers.push_back("HLT_2mu6_bBmumux_BcmumuDsloose_delayed");  ////1459
alg->triggers.push_back("L1_XE80");  ////1443
alg->triggers.push_back("HLT_2mu6_bBmumuxv2");  ////1434
alg->triggers.push_back("L1_3J15.0ETA25");  ////1361
alg->triggers.push_back("L1_TAU20_2TAU12_XE35");  ////1335
alg->triggers.push_back("HLT_xe100_mht_wEFMu_L1XE50");  ////1248
alg->triggers.push_back("HLT_mu20_ivarmedium_L1MU10_2J15_J20");  ////1186
alg->triggers.push_back("HLT_j0_0i1c500m2000TLA_delayed");  ////1176
alg->triggers.push_back("HLT_mu10_mu6_bBmumux_BcmumuDsloose_delayed");  ////1174
alg->triggers.push_back("HLT_mu6_mu4_bUpsimumu");  ////1170
alg->triggers.push_back("HLT_2mu15");  ////1168
alg->triggers.push_back("HLT_j400_a10_lcw_L1J100");  ////1154
alg->triggers.push_back("HLT_mu6_mu4_bJpsimumu_Lxy0");  ////1139
alg->triggers.push_back("HLT_mu10_mu6_bJpsimumu");  ////1134
alg->triggers.push_back("HLT_mu4_mu4_idperf_bJpsimumu_noid");  ////1132
alg->triggers.push_back("HLT_xe100_mht_L1XE55");  ////1123
alg->triggers.push_back("HLT_mu20_l2idonly_mu6noL1_nscan03_bTau");  ////1114
alg->triggers.push_back("L1_3J15");  ////1103
alg->triggers.push_back("HLT_j65_bmv2c2070_split_3j65_L14J15");  ////1090
alg->triggers.push_back("HLT_j380_lcw_jes");  ////1087
alg->triggers.push_back("HLT_j70_bmv2c2077_split_3j70_L14J15");  ////1085


/*
*/
  job.algsAdd(alg);
  alg->output_name = output_name;

//  EL::DirectDriver driver;
//  driver.submit(job, submitDir);
if(submittoGrid){
 EL::PrunDriver driver;
    driver.options()->setString( "nc_outputSampleName" , "user.abarton.AOD.f705_m1606.run00300345to00302925.trigntuple.6");
// driver.options()->setString(EL::Job::optGridNFilesPerJob,  "1");
  driver.submitOnly (job, submitDir);
}else{
  EL::DirectDriver driver;
  driver.submit(job, submitDir);
}

}
