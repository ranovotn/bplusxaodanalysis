void ATestRunSubmitBplus()
  {
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE 
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  TString grlFilePath="";
  Int_t year = 15;
  TString stream = "main";
 
  std::string submitDir = Form("/tmp/outputnewntupleBplus%d_%s",year,stream.Data());
  xAOD::Init().ignore();
  SH::SampleHandler sh;

  

  if(year==15){
    grlFilePath = "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml";
    SH::scanRucio (sh, "data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY5.grp15_v01_p3704");
  }else if(year==16){
	grlFilePath = "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
	if(stream=="main") SH::scanRucio (sh, "data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY5.grp16_v01_p3704");
	else SH::scanRucio (sh, "data16_13TeV.periodAllYear.physics_BphysDelayed.PhysCont.DAOD_BPHY5.grp16_v01_p3704");
  }else if(year==17){
	grlFilePath = "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
	if(stream=="main") SH::scanRucio (sh, "data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY5.grp17_v02_p3704");
	else SH::scanRucio (sh, "data17_13TeV.periodAllYear.physics_BphysLS.PhysCont.DAOD_BPHY5.grp17_v02_p3704");
  }else if(year==18){
	grlFilePath = "GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
	if(stream=="main") SH::scanRucio (sh, "data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_BPHY5.grp18_v01_p3704");
	else SH::scanRucio (sh, "data18_13TeV.periodAllYear.physics_BphysLS.PhysCont.DAOD_BPHY5.grp18_v01_p3704");
  }

  sh.setMetaString("nc_tree", "CollectionTree");
  sh.print();

  EL::Job job;
  job.sampleHandler(sh);
  // job.options()->setDouble(EL::Job::optSkipEvents, 1);
  // job.options()->setDouble(EL::Job::optMaxEvents, 10);

  const char *output_name = "DefaultOutput";
  EL::OutputStream output(output_name);
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_name);
  job.algsAdd(ntuple);

  AnaBaseCalibMuonsProvider * calibAlg = new AnaBaseCalibMuonsProvider();
  calibAlg->m_inputMuonsKey     = "Muons";
  calibAlg->m_calibMuonsKey     = "CalibMuons";
  calibAlg->m_MCS_Year          = "2016";
  calibAlg->m_MCS_AthenaRelease = "21";      
  job.algsAdd(calibAlg);

      
      
  BplusNtupleMaker *alg = new BplusNtupleMaker;
//  alg->grl_files.push_back("$ROOTCOREBIN/data/BsxAODAnalysis/data16_13TeV.periodAllYear_DetStatus-v84-pro20-16_DQDefects-00-02-04_PHYS_StandardGRL_All_Good.xml");

  alg->grl_files.emplace_back(grlFilePath);
  alg->m_useCalibArea = false;
  alg->use_grl = true;
  alg->m_truthOnly =false;
  alg->m_truthTreeOnly =false;
  alg->m_doL1TriggerTree = false;
  alg->DisableTrigger = false;
  alg->JpsiCode = 999443;
  alg->m_DisableAllCands = false;
  alg->cut_kaon_pT   = 1000.0f;
  alg->cut_Bmass_lower   = 4800.0f;
  alg->cut_Bmass_upper   = 5900.0f;

  #include "data18uptoOctTriggerListTrim.txt"
  #include "2016mergedTriggerlist2.txt"
  #include "Data15Triggers.txt"
  #include "2017MergedList.txt"

  job.algsAdd(alg);
  alg->output_name = output_name;

  EL::PrunDriver driver;
  TString outputFileName = Form("user.ranovotn.data%d_BPHY5_%s.Bplus_v2",year,stream.Data());

  driver.options()->setString("nc_outputSampleName", outputFileName.Data());
  driver.submitOnly (job, submitDir);


}
