void Truth()
  {
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE 
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  std::string submitDir = "outputnewntuple";
  xAOD::Init().ignore();
  SH::SampleHandler sh;

  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // inputFilePath needsto be changed to your local input file 

  //SH::readFileList (sh, "sample", "filelist.txt");
//  SH::scanDQ2 (sh, "user.abarton.mc15_13TeV.300999.Bplus.e4064_a766_a769_r6264.BPHYS2.NoEventCut_EXT0/");


  SH::scanRucio (sh,  "user.abarton.mc15_13TeV.300438.Pythia8BPhotospp_A14_CTEQ6L1_Bs_Jpsimu3p5mu3p5_phi.evgen.TRUTH0.5_EXT0/");
  
  sh.setMetaString("nc_tree", "CollectionTree");
  sh.print();

  EL::Job job;
  job.sampleHandler(sh);
  // job.options()->setDouble(EL::Job::optSkipEvents, 1);
  // job.options()->setDouble(EL::Job::optMaxEvents, 10);

  const char *output_name = "DefaultOutput";
  EL::OutputStream output(output_name);
  job.outputAdd(output);
  EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_name);
  job.algsAdd(ntuple);

  BsNtupleMaker *alg = new BsNtupleMaker;
  alg->use_grl = false;
  alg->m_truthOnly =false;
  alg->m_truthTreeOnly =true;
  alg->JpsiCode = 999443;


//MC15


/*
*/
  job.algsAdd(alg);
  alg->output_name = output_name;
//  const char* grlFilePath = "$ROOTCOREBIN/data/BplusxAODAnalysis/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.xml";
//  alg->grl_files.emplace_back(grlFilePath);
//  alg->use_grl = true;
  // alg->cut_chi2_ndof =    5.0f;
  // alg->cut_kaon_pT   = 1000.0f;
  //alg->print_trigger_regexp = "HLT_[0-9]?mu.*"; // use empty string (or remove this line) to silence trigger output


//  EL::DirectDriver driver;
//  driver.submit(job, submitDir);

 EL::PrunDriver driver;
//  driver.options()->setString("nc_outputSampleName", "user.abarton.data15_13TeV.%in:name[5]%.%in:name[4]%.ntup.newgrl_newntuple.3");
//  driver.options()->setString("nc_outputSampleName", "user.abarton.data15_13TeV.%in:name[5]%.%in:name[4]%.BsPi.ntup.3");
// driver.options()->setString("nc_outputSampleName", "user.abarton.mc15_13TeV.300438.Bs_Jpsimu3p5mu3p5_phi.phy5.3.ntpl.4");
    driver.options()->setString( "nc_outputSampleName" , "user.abarton.mc15_13TeV.300438.evgen.TRUTH0.ntpl.2");
//  driver.options()->setString(EL::Job::optGridNFilesPerJob,  "MAX");
  driver.submitOnly (job, submitDir);


}
